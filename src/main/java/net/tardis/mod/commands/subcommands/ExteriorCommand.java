package net.tardis.mod.commands.subcommands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.ISuggestionProvider;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.dimension.DimensionType;
import net.minecraftforge.fml.server.ServerLifecycleHooks;
import net.tardis.mod.commands.permissions.PermissionEnum;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.exceptions.NoDimensionFoundException;
import net.tardis.mod.exceptions.NoPlayerFoundException;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.helper.TeleportUtil;
import net.tardis.mod.tileentities.ConsoleTile;

public class ExteriorCommand extends TCommand {
    private static final ExteriorCommand CMD = new ExteriorCommand();

    public ExteriorCommand() {
        super(PermissionEnum.EXTERIOR);
    }

    public static ArgumentBuilder<CommandSource, ?> register(CommandDispatcher<CommandSource> dispatcher) {
        return Commands.literal("exterior")
                .then(Commands.argument("username", StringArgumentType.string())
                        .suggests((context, builder) -> ISuggestionProvider.suggest(ServerLifecycleHooks.getCurrentServer().getOnlinePlayerNames(), builder))
                        .executes(CMD));
    }

    @Override
    public int run(CommandContext<CommandSource> context) throws CommandSyntaxException {
        CommandSource source = context.getSource();
        String username = context.getArgument("username", String.class);

        if (canExecute(source)) {
            try {
                ServerPlayerEntity sourcePlayer = source.asPlayer();
                ConsoleTile console = TardisHelper.getConsole(context.getSource().getServer(), TardisHelper.getTardisDimension(username));
                BlockPos tardisLocation = console.getLocation();
                DimensionType tardisDimension = console.getDimension();
                TeleportUtil.teleportEntity(sourcePlayer, tardisDimension, tardisLocation.getX() + 2, tardisLocation.getY(), tardisLocation.getZ());
                source.sendFeedback(new TranslationTextComponent("message.tardis.exterior_command", username), true);
            } catch (NoPlayerFoundException | NoDimensionFoundException e) {
                source.sendErrorMessage(new StringTextComponent(e.getMessage()));
            }
        } else {
            source.sendErrorMessage(new StringTextComponent(Constants.Message.NO_PERMISSION));
        }

        return Command.SINGLE_SUCCESS;
    }
}

