package net.tardis.mod.ars;

import net.minecraft.util.math.BlockPos;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.ARSPiece.CooridorType;
import net.tardis.mod.registries.TardisRegistries;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Bus.MOD)
public class ARSPieces {
	
	@SubscribeEvent
	public static void register(FMLCommonSetupEvent event) {
		register("straight", new ARSPiece("straight", new BlockPos(3, 2, 21), CooridorType.CORRIDOR_3x4));
		register("t3", new ARSPiece("t_3", new BlockPos(6, 2, 9), CooridorType.CORRIDOR_3x4));
		register("small", new ARSPiece("small", new BlockPos(10, 2, 26), CooridorType.CORRIDOR_3x3));
		register("library", new ARSPiece("library", new BlockPos(13, 2, 29), CooridorType.CORRIDOR_3x4));
	}
	
	public static ARSPiece register(String name, ARSPiece piece) {
		TardisRegistries.ARS_PIECES.register(name, piece);
		return piece;
	}
}
