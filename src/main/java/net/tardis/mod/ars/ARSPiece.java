package net.tardis.mod.ars;

import net.minecraft.block.Blocks;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.gen.feature.template.PlacementSettings;
import net.minecraft.world.gen.feature.template.Template;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.Tardis;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.registries.IRegisterable;

public class ARSPiece implements IRegisterable<ARSPiece>{
	
	private ResourceLocation name;
	
	private ResourceLocation struct;
	private BlockPos offset;
	private CooridorType type;
	
	public ARSPiece(ResourceLocation loc, BlockPos offset, CooridorType type) {
		this.struct = loc;
		this.offset = offset;
		this.type = type;
	}
	
	public ARSPiece(String loc, BlockPos offset, CooridorType type) {
		this(new ResourceLocation(Tardis.MODID, "tardis/structures/ars/" + loc), offset, type);
	}
	
	public void spawn(ServerWorld world, BlockPos pos, Direction dir) {
		Template temp = world.getStructureTemplateManager().getTemplate(struct);
		
		for(int x = -1; x <= 1; ++x) {
			for(int y = -1; y < (type == CooridorType.CORRIDOR_3x3 ? 2 : 3); ++y) {
				if(dir == Direction.SOUTH || dir == Direction.NORTH)
					world.setBlockState(pos.add(x, y, 0), Blocks.AIR.getDefaultState());
				else world.setBlockState(pos.add(0, y, x), Blocks.AIR.getDefaultState());
			}
		}
		
		BlockPos offset = Helper.rotateBlockPos(this.offset, dir);
		
		if(temp != null) {
			PlacementSettings set = new PlacementSettings().setIgnoreEntities(false).setRotation(Helper.getRotationFromDirection(dir));
			temp.addBlocksToWorld(world, pos.subtract(offset), set);
			
		}
		else System.err.println("WARNING: Could not load structure " + this.struct);
		
	}

	@Override
	public ARSPiece setRegistryName(ResourceLocation regName) {
		this.name = regName;
		return this;
	}

	@Override
	public ResourceLocation getRegistryName() {
		return this.name;
	}
	
	public static enum CooridorType{
		CORRIDOR_3x3,
		CORRIDOR_3x4;
	}
}
