package net.tardis.mod.texturevariants;

import net.tardis.mod.misc.TexVariant;

public class TextureVariants {

	public static final TexVariant[] TRUNK = {
			new TexVariant("trunk_base", "exterior.trunk.normal"),
			new TexVariant("trunk_dark", "exterior.trunk.dark")
	};
}
