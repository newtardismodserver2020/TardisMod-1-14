package net.tardis.mod.world;

import java.util.Random;
import java.util.function.Function;

import com.mojang.datafixers.Dynamic;

import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.GenerationSettings;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.ProbabilityConfig;
import net.tardis.mod.blocks.TBlocks;

public class XionCrystalFeature extends Feature<ProbabilityConfig> {

	public XionCrystalFeature(Function<Dynamic<?>, ? extends ProbabilityConfig> configFactoryIn) {
		super(configFactoryIn);
	}

	@Override
	public boolean place(IWorld worldIn, ChunkGenerator<? extends GenerationSettings> generator, Random rand, BlockPos pos, ProbabilityConfig config) {
		BlockPos newPos = pos;
		boolean generated = false;
		for(int i = 0; i < 36; ++i) {
			newPos = pos.add(rand.nextInt(10), 0, rand.nextInt(10));
			for(int y = newPos.getY(); y > 0; --y) {
				newPos = new BlockPos(newPos.getX(), y, newPos.getZ());
				if((worldIn.getBlockState(newPos).isAir(worldIn, newPos) || worldIn.getFluidState(newPos).isTagged(FluidTags.WATER)) && worldIn.getBlockState(newPos.down()).isSolid()) {
					worldIn.setBlockState(newPos, TBlocks.xion_crystal.getDefaultState()
							.with(BlockStateProperties.WATERLOGGED, worldIn.getFluidState(newPos).isTagged(FluidTags.WATER)), 2);
					generated = true;
					break;
				}
			}
		}
		return generated;
	}

}
