package net.tardis.mod.datagen;

import net.minecraft.block.Block;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.event.lifecycle.GatherDataEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.tags.TardisBlockTags;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Bus.MOD)
public class DataGen {
	
	public static TagGen<Block> ARS_DATAGEN = new TagGen<Block>(TardisBlockTags.ARS);
	public static BlockstateTagGen BLOCKSTATE_GEN = new BlockstateTagGen();
	
	@SubscribeEvent
	public static void onDataGen(GatherDataEvent event) {
		
		ARS_DATAGEN.create(event.getGenerator());
		//BLOCKSTATE_GEN.create(event.getGenerator());
	}

}
