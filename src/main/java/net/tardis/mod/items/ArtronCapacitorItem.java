package net.tardis.mod.items;

import net.minecraft.item.Item;
import net.tardis.mod.properties.Prop;

public class ArtronCapacitorItem extends Item {

	private float storage = 32F;
	private float rechargeModifier = 1F;
	
	public ArtronCapacitorItem(float storage, float rechargeMod) {
		super(Prop.Items.ONE);
		this.storage = storage;
		this.rechargeModifier = rechargeMod;
	}
	
	public float getRechangeModifier() {
		return this.rechargeModifier;
	}
	
	public float getMaxStorage() {
		return this.storage;
	}

}
