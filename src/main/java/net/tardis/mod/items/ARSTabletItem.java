package net.tardis.mod.items;

import javax.annotation.Nullable;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.concurrent.TickDelayedTask;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.ARSPiece;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.contexts.gui.GuiItemContext;
import net.tardis.mod.properties.Prop;
import net.tardis.mod.registries.TardisRegistries;

public class ARSTabletItem extends Item {

	public ARSTabletItem() {
		super(Prop.Items.ONE);
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
		if(worldIn.isRemote)
			Tardis.proxy.openGUI(Constants.Gui.ARS_TABLET, new GuiItemContext(playerIn.getHeldItem(handIn)));
		return super.onItemRightClick(worldIn, playerIn, handIn);
	}

	@Override
	public ActionResultType onItemUse(ItemUseContext context) {
		if(context.getHand() == context.getPlayer().getActiveHand()) {
			if(!context.getWorld().isRemote) {
				if(context.getWorld().getBlockState(context.getPos()).getBlock() == TBlocks.corridor_spawn) {
					context.getWorld().getServer().enqueue(new TickDelayedTask(1, () -> {
						ARSPiece piece = getSelectedPiece(context.getItem());
						if(piece != null) {
							piece.spawn((ServerWorld)context.getWorld(), context.getPos(), context.getFace().getOpposite());
						}
					}));
					return ActionResultType.SUCCESS;
				}
			}
		}
		return context.getWorld().getBlockState(context.getPos()).getBlock() == TBlocks.corridor_spawn ? ActionResultType.SUCCESS : super.onItemUse(context);
	}
	
	@Nullable
	public static ARSPiece getSelectedPiece(ItemStack stack) {
		CompoundNBT tag = stack.getOrCreateTag();
		if(tag.contains("piece"))
			return TardisRegistries.ARS_PIECES.getValue(new ResourceLocation(tag.getString("piece")));
		return null;
	}
	
	public static void setPiece(ItemStack stack, ARSPiece piece) {
		stack.getOrCreateTag().putString("piece", piece.getRegistryName().toString());
	}

}
