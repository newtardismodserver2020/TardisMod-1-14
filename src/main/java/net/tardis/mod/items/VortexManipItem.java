package net.tardis.mod.items;


import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.tardis.mod.Tardis;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.itemgroups.TItemGroups;

public class VortexManipItem extends BaseItem{
	
	public VortexManipItem(){
		super(new Properties().maxStackSize(1).group(TItemGroups.MAIN));
		
		this.addPropertyOverride(new ResourceLocation("open"), (stack, worldIn, entityIn) -> {
			if (getItemTag(stack) == null || !getItemTag(stack).contains("open")) {
				return 0F; //Initiates item as closed by default
			}
			return getOpenState(stack);
		});
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity player, Hand handIn){
		ItemStack stack = player.getHeldItem(handIn);
		if (!worldIn.isRemote) {
			if (getOpenState(stack) == 0) { //Set open model
				setOpen(stack, 1);
			}
		}
		if(worldIn.isRemote)
			Tardis.proxy.openGUI(Constants.Gui.VORTEX_MAIN, null);
		return new ActionResult<ItemStack>(ActionResultType.SUCCESS, stack);
	}
	
	public static CompoundNBT getItemTag(ItemStack stack) {
		stack.getOrCreateTag().putInt("open", 0);
		return stack.getTag();
	}
	
	public static int getOpenState(ItemStack stack) {
		return getItemTag(stack).getInt("open");
	}

	public static void setOpen(ItemStack stack, int amount) {
		getItemTag(stack).putInt("open", amount);
	}
	
	@Override
	public boolean shouldCauseReequipAnimation(ItemStack oldStack, ItemStack newStack, boolean slotChanged) {
		return false;
	}
	
	//Set model state to closed upon gui closed - temp solution
	@Override
	public void inventoryTick(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected) {
		if (getOpenState(stack) == 1) {
			if (entityIn.ticksExisted % 200 == 0) {
				setOpen(stack, 0);
			}
		}
		super.inventoryTick(stack, worldIn, entityIn, itemSlot, isSelected);
	}
	
}
