package net.tardis.mod.cap.items;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.Capability.IStorage;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.tileentities.inventory.BatteryInventory;

public interface IVortexCap extends INBTSerializable<CompoundNBT>{

	float getShieldHealth();
	void setShieldHealth(float health);
	void tick();
	float getDischargeAmount();
	BatteryInventory getBatteryInventory(ItemStack stack);
	
	public static class Storage implements IStorage<IVortexCap>{

		@Override
		public INBT writeNBT(Capability<IVortexCap> capability, IVortexCap instance, Direction side) {
			return instance.serializeNBT();
		}

		@Override
		public void readNBT(Capability<IVortexCap> capability, IVortexCap instance, Direction side, INBT nbt) {
			instance.deserializeNBT((CompoundNBT)nbt);
		}
		
	}
	
	public static class Provider implements ICapabilitySerializable<CompoundNBT>{

		private IVortexCap vortex;
		
		public Provider() {
			vortex = new VortexCapability();
		}
		
		@Override
		public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
			return cap == Capabilities.VORTEX_MANIP ? (LazyOptional<T>) LazyOptional.of(() -> vortex) : LazyOptional.empty();
		}

		@Override
		public CompoundNBT serializeNBT() {
			return vortex.serializeNBT();
		}

		@Override
		public void deserializeNBT(CompoundNBT nbt) {
			vortex.deserializeNBT(nbt);;
		}}
}
