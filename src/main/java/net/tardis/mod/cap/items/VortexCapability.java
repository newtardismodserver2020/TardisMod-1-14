package net.tardis.mod.cap.items;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.world.World;
import net.tardis.mod.artron.IArtronBattery;
import net.tardis.mod.tileentities.inventory.BatteryInventory;

public class VortexCapability implements IVortexCap {
	private World world;
	private ItemStack stack;
	public BatteryInventory batteryInv = new BatteryInventory();
	private float dischargeAmount;
	
	public VortexCapability() {}
	
	public VortexCapability (World world, ItemStack stack) {
		this.world = world;
		this.stack = stack;
	}
	
	@Override
	public CompoundNBT serializeNBT() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		// TODO Auto-generated method stub

	}

	@Override
	public float getShieldHealth() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setShieldHealth(float health) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void tick() {
		//Artron discharge
		if(!world.isRemote && world.getGameTime() % 200 == 0) {
			for(int index = 0; index < batteryInv.getSizeInventory(); ++index) {
				ItemStack slotStack = batteryInv.getStackInSlot(index);
				if(slotStack.getItem() instanceof IArtronBattery) {
					if (((IArtronBattery)slotStack.getItem()).getCharge(stack) > 0) {
						((IArtronBattery)slotStack.getItem())
						.discharge(slotStack, dischargeAmount);
					}
				}
			}
		}
	}
	
	@Override
	public float getDischargeAmount() {
		return this.dischargeAmount;
	}
	
	@Override
	public BatteryInventory getBatteryInventory(ItemStack stack) {
		return this.batteryInv;
	}

}
