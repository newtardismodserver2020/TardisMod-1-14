package net.tardis.mod.cap;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.BlockState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.nbt.LongNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.common.util.Constants.NBT;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.TardisBlockProperties;
import net.tardis.mod.tileentities.ConsoleTile;

public class LightCapability implements ILightCap{

	private List<BlockPos> lights = new ArrayList<BlockPos>();
	private Chunk chunk;
	private int lightLevel = 0;
	
	public LightCapability(Chunk chunk) {
		this.chunk = chunk;
	}
	
	public LightCapability() {}
	
	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		ListNBT list = new ListNBT();
		for(BlockPos pos : lights) {
			list.add(new LongNBT(pos.toLong()));
		}
		tag.put("light", list);
		tag.putInt("light_level", lightLevel);
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		ListNBT list = nbt.getList("light", NBT.TAG_LONG);
		for(INBT pos : list) {
			this.lights.add(BlockPos.fromLong(((LongNBT)pos).getLong()));
		}
		this.lightLevel = nbt.getInt("light_level");
	}

	@Override
	public List<BlockPos> getLightPoses() {
		return this.lights;
	}

	@Override
	public void addLightPos(BlockPos pos) {
		this.lights.add(pos);
		
		if(!chunk.getWorld().isRemote) {
			BlockState state = chunk.getWorld().getBlockState(pos);
			if(state.has(TardisBlockProperties.LIGHT))
				chunk.getWorld().setBlockState(pos, state.with(TardisBlockProperties.LIGHT, this.lightLevel));
		}
	}

	@Override
	public void onLoad() {
		if(this.chunk.getWorld().isBlockLoaded(TardisHelper.TARDIS_POS)) {
			TileEntity te = this.chunk.getWorld().getTileEntity(TardisHelper.TARDIS_POS);
			if(te instanceof ConsoleTile) {
				this.setLight(((ConsoleTile)te).getInteriorManager().getLight());
			}
		}
	}

	@Override
	public void setLight(int level) {
		
		for(BlockPos pos : this.lights) {
			BlockState state = chunk.getWorld().getBlockState(pos);
			if(state.has(TardisBlockProperties.LIGHT)) {
				chunk.getWorld().setBlockState(pos, state.with(TardisBlockProperties.LIGHT, level), 3);
			}
		}
		this.lightLevel = level;
	}

}
