package net.tardis.mod.misc;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;

public class GuiContext {

	private BlockPos pos;
	private ResourceLocation dim;
	
	public GuiContext(BlockPos pos, ResourceLocation dim) {
		this.pos = pos;
		this.dim = dim;
	}
	
	public GuiContext(){}
	
	public BlockPos getPos() {
		return pos;
	}

	public ResourceLocation getDimension() {
		return dim;
	}

}
