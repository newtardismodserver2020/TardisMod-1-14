package net.tardis.mod.misc.vm;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.containers.TContainers;
import net.tardis.mod.containers.VMContainer;
import net.tardis.mod.tileentities.inventory.BatteryInventory;

/**
 * Created by 50ap5ud5
 * on 5 May 2020 @ 8:04:17 pm
 */
public class BatterySubFunction implements ISubFunction {

	@Override
	public void onActivated(World world, PlayerEntity player) {
		if (!world.isRemote) {
			world.getCapability(Capabilities.VORTEX_MANIP).ifPresent((cap) -> {
				BatteryInventory inv = cap.getBatteryInventory(player.getActiveItemStack());
				player.openContainer(new INamedContainerProvider() {
					@Override
					public Container createMenu(int id, PlayerInventory playerInv, PlayerEntity player) {
						return new VMContainer(TContainers.VORTEX_M_BATTERY, id, playerInv, inv, 1);
					}

					@Override
					public ITextComponent getDisplayName() {
						return inv.getName();
					}});
			});
		}
		
	}

	@Override
	public String getNameKey() {
		return new TranslationTextComponent("subfunction.vm.battery").getFormattedText();
	}

	@Override
	public Boolean stateComplete() {
		return true;
	}

}
