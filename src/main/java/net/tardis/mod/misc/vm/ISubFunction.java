package net.tardis.mod.misc.vm;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.world.World;

/**
 * Created by 50ap5ud5
 * on 5 May 2020 @ 8:03:22 pm
 */
public interface ISubFunction {
	
	void onActivated(World world,PlayerEntity player);

	String getNameKey();
	Boolean stateComplete();
}
