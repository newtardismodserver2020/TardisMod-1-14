package net.tardis.mod.misc;

import net.minecraft.item.Item;

public interface INeedItem {

    Item getItem();

}
