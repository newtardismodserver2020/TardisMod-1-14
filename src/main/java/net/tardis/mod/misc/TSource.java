package net.tardis.mod.misc;

import net.minecraft.entity.LivingEntity;
import net.minecraft.util.DamageSource;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;

public class TSource extends DamageSource {

    private String message;
    private boolean blockable;

    public TSource(String name, boolean blockable) {
        super(name);
        this.message = "damagesrc.tardis." + name;
        this.blockable = blockable;
    }

    public TSource(String name) {
        this(name, false);
    }

    @Override
    public ITextComponent getDeathMessage(LivingEntity entity) {
        return new TranslationTextComponent(message, entity.getName().getUnformattedComponentText());
    }

    @Override
    public boolean isUnblockable() {
        return !blockable;
    }

}