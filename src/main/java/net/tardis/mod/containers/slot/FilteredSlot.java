package net.tardis.mod.containers.slot;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;

public class FilteredSlot extends Slot{

	private IFilter filter;
	
	public FilteredSlot(IInventory inventoryIn, int index, int xPosition, int yPosition, IFilter filter) {
		super(inventoryIn, index, xPosition, yPosition);
		this.filter = filter;
	}

	@Override
	public boolean isItemValid(ItemStack stack) {
		return filter.isValid(stack);
	}
	
	public static interface IFilter{
		boolean isValid(ItemStack item);
	}

}
