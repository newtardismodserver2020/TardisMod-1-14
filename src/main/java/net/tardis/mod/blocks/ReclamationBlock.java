package net.tardis.mod.blocks;

import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;
import net.tardis.mod.containers.OutputOnlyContainer;
import net.tardis.mod.misc.IDontBreak;
import net.tardis.mod.properties.Prop;
import net.tardis.mod.tileentities.ReclamationTile;

public class ReclamationBlock extends TileBlock implements IDontBreak{

	public ReclamationBlock() {
		super(Prop.Blocks.BASIC_TECH.get().hardnessAndResistance(999F));
	}

	@Override
	public boolean onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
		if(!worldIn.isRemote) {
			NetworkHooks.openGui((ServerPlayerEntity)player, new INamedContainerProvider() {

				@Override
				public Container createMenu(int id, PlayerInventory inv, PlayerEntity p_createMenu_3_) {
					return OutputOnlyContainer.createGeneric9x4(id, inv, (IInventory)worldIn.getTileEntity(pos));
				}

				@Override
				public ITextComponent getDisplayName() {
					return new TranslationTextComponent("container.tardis.reclamation_unit");
				}});
		}
		return true;
	}
	
	@Override
	public void onReplaced(BlockState state, World worldIn, BlockPos pos, BlockState newState, boolean isMoving) {
		ReclamationTile tileEntity = (ReclamationTile) worldIn.getTileEntity(pos); 
		 if (tileEntity != null && state.getBlock() != newState.getBlock()) {
			 InventoryHelper.dropInventoryItems(worldIn, pos, (ReclamationTile) tileEntity); //Useful for when something breaks this for whatever reason
			 worldIn.removeTileEntity(pos);
			 worldIn.playSound(null, pos, SoundEvents.BLOCK_ANVIL_USE, SoundCategory.BLOCKS, 0.75F, 1F);
		 }   
	}

}
