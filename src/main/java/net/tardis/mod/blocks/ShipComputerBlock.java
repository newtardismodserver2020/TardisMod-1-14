package net.tardis.mod.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.container.ChestContainer;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemStack;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.items.TItems;
import net.tardis.mod.properties.Prop;
import net.tardis.mod.tileentities.ShipComputerTile;

public class ShipComputerBlock extends TileBlock {

	public ShipComputerBlock() {
		super(Prop.Blocks.BASIC_TECH.get());
	}

	@Override
	public boolean onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
		if(!worldIn.isRemote) {
			TileEntity te = worldIn.getTileEntity(pos);
			if(te instanceof ShipComputerTile) {
				ShipComputerTile comp = (ShipComputerTile)te;
				ItemStack held = player.getHeldItem(handIn);
				
				if(comp.getSchematic() != null && held.getItem() == TItems.SONIC)
					held.getCapability(Capabilities.SONIC_CAPABILITY).ifPresent(cap -> {
						cap.getSchematics().add(comp.getSchematic());
						comp.setSchematic(null);
						//Remove schematic from computer
						player.sendStatusMessage(new TranslationTextComponent("status.tardis.computer.downloaded"), true);
					});
				else NetworkHooks.openGui((ServerPlayerEntity)player, new INamedContainerProvider() {

					@Override
					public Container createMenu(int id, PlayerInventory inv, PlayerEntity p_createMenu_3_) {
						return ChestContainer.createGeneric9X3(id, inv, comp);
					}

					@Override
					public ITextComponent getDisplayName() {
						return new TranslationTextComponent("container.tardis.ship_computer");
					}});
			}
		}
		return true;
	}

	@Override
	public BlockState getStateForPlacement(BlockItemUseContext context) {
		return this.getDefaultState().with(BlockStateProperties.HORIZONTAL_FACING, context.getPlayer().getHorizontalFacing().getOpposite());
	}

	@Override
	protected void fillStateContainer(Builder<Block, BlockState> builder) {
		builder.add(BlockStateProperties.HORIZONTAL_FACING);
	}
}
