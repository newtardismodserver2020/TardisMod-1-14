package net.tardis.mod.blocks;

import net.minecraft.block.SoundType;
import net.minecraft.block.WallBlock;

/**
 * This extends the Cobblestone Wall type.
 * @author 50ap5ud5
 *
 */

public class ModWallBlock extends WallBlock{

    public ModWallBlock(Properties prop, SoundType sound, float hardness, float resistance) {
        super(prop.sound(sound).hardnessAndResistance(hardness, resistance));
    }
    

}
