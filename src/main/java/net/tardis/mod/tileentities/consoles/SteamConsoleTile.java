package net.tardis.mod.tileentities.consoles;

import net.minecraft.util.math.AxisAlignedBB;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.TTiles;

public class SteamConsoleTile extends ConsoleTile{
	
	public static final AxisAlignedBB RENDER_BOX = new AxisAlignedBB(-2, 0, -2, 2, 5, 2);

	public SteamConsoleTile() {
		super(TTiles.CONSOLE_STEAM);
	}
	
	@Override
	public AxisAlignedBB getRenderBoundingBox() {
		return RENDER_BOX.offset(this.getPos());
	}

}
