package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.block.BlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.blocks.BrokenExteriorBlock;
import net.tardis.mod.blocks.TBlocks;

public class BrokenTardisSpawn {
	
	private BlockPos pos;
	
	public BrokenTardisSpawn(BlockPos pos) {
		this.pos = pos;
	}
	
	public static void encode(BrokenTardisSpawn mes, PacketBuffer buf) {
		buf.writeBlockPos(mes.pos);
	}
	
	public static BrokenTardisSpawn decode(PacketBuffer buf) {
		return new BrokenTardisSpawn(buf.readBlockPos());
	}
	
	public static void handle(BrokenTardisSpawn mes, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> {
			BlockState state = ((ClientWorld)Minecraft.getInstance().world).getBlockState(mes.pos);
			if(state.getBlock() == TBlocks.broken_exterior) {
				BrokenExteriorBlock.spawnHappyPart((ClientWorld)Minecraft.getInstance().world, mes.pos, ((ClientWorld)Minecraft.getInstance().world).rand);
			}
		});
		cont.get().setPacketHandled(true);
	}

}
