package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import com.google.common.collect.Lists;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.network.NetworkEvent;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.tileentities.ConsoleTile;

public class TelepathicMessage {

    private static final TranslationTextComponent TRANS_NO_BIOME = new TranslationTextComponent("status.tardis.telepathic.no_biome");
    private static final TranslationTextComponent TRANS = new TranslationTextComponent("status.tardis.telepathic.sucess");
    ResourceLocation name;

    public TelepathicMessage(ResourceLocation loc) {
        this.name = loc;
    }

    public static void encode(TelepathicMessage mes, PacketBuffer buf) {
        buf.writeResourceLocation(mes.name);
    }

    public static TelepathicMessage decode(PacketBuffer buf) {
        return new TelepathicMessage(buf.readResourceLocation());
    }

    public static void handle(TelepathicMessage mes, Supplier<NetworkEvent.Context> cont) {
        cont.get().enqueueWork(() -> {
            TileEntity te = cont.get().getSender().world.getTileEntity(TardisHelper.TARDIS_POS);
            if (te instanceof ConsoleTile) {
                ConsoleTile console = (ConsoleTile) te;
                ServerWorld world = console.getWorld().getServer().getWorld(console.getDimension());
                if (world != null) {
                    BlockPos dest = console.getDestination();
                    	PlayerEntity player = cont.get().getSender();
                    	BlockPos biome = world.getChunkProvider().getChunkGenerator().getBiomeProvider().findBiomePosition(dest.getX() >> 4, dest.getZ() >> 4, 500, Lists.newArrayList(ForgeRegistries.BIOMES.getValue(mes.name)), world.rand);
                    	if(biome != null) {
                    		console.setDestination(console.getDestinationDimension(), biome.add(0, 64, 0));
                            player.sendStatusMessage(TRANS, true);
                    	}
                    	else player.sendStatusMessage(TRANS_NO_BIOME, true);
                }
            }
        });
        cont.get().setPacketHandled(true);
    }

}
