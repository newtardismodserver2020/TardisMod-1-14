package net.tardis.mod.client.guis.vm;


import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.guis.vm.widgets.DisabledTextFieldWidget;
import net.tardis.mod.constants.Constants;


public class VortexMChargeStatusGui extends VortexMFunctionScreen{
	
	private DisabledTextFieldWidget chargeValue;
	private DisabledTextFieldWidget shieldValue;
	private Button back;
	
	public VortexMChargeStatusGui() {
	}
	
	
	@Override
	public void init() {
		super.init();
		String backButton = new TranslationTextComponent("button.vm.back").getFormattedText();
		
		chargeValue = new DisabledTextFieldWidget(this.font, super.getMinX() + 45, super.getMaxY() + 85, 50, this.font.FONT_HEIGHT, "test");
		shieldValue = new DisabledTextFieldWidget(this.font, super.getMinX() + 45, super.getMaxY() + 105, 50, this.font.FONT_HEIGHT, "test");
		back = new Button(super.getMinX(),this.getMaxY() + 35, this.font.getStringWidth(backButton) + 8, this.font.FONT_HEIGHT + 11, backButton, new Button.IPressable() {
			@Override
			public void onPress(Button button) {
				Tardis.proxy.openGUI(Constants.Gui.VORTEX_MAIN, null);
			}
		});
		this.buttons.clear();
		this.addButton(chargeValue); //addButton also adds other widget types, mapping name can be misleading
		this.addButton(shieldValue);
		this.addButton(back);
		chargeValue.setFocused2(true);
	}
	
	@Override
	public void renderBackground() {
		super.renderBackground();
	}
	
	@Override
    public void render(int mouseX, int mouseY, float partialTicks) {
        super.render(mouseX, mouseY, partialTicks);
		drawCenteredString(this.font, "Charge Status", super.getMinX() + 73, super.getMaxY() + 40, 0xFFFFFF);
    }
	
}
