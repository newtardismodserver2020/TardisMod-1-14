package net.tardis.mod.client.guis;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.util.text.StringTextComponent;
import net.tardis.mod.ars.ARSPiece;
import net.tardis.mod.client.guis.widgets.TextButton;
import net.tardis.mod.misc.GuiContext;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.UpdateARSTablet;
import net.tardis.mod.registries.TardisRegistries;

public class ArsTabletScreen extends Screen {
	
	
	public ArsTabletScreen() {
		super(new StringTextComponent(""));
	}
	
	public ArsTabletScreen(GuiContext cont) {
		this();
	}

	@Override
	protected void init() {
		super.init();
		
		int id = 0;
		
		for(ARSPiece p : TardisRegistries.ARS_PIECES.getRegistry().values()) {
			this.addButton(new TextButton(width / 2, height / 2 - (id * this.font.FONT_HEIGHT) + 5, p.getRegistryName().toString(), but -> { 
				Network.INSTANCE.sendToServer(new UpdateARSTablet(p.getRegistryName()));
				Minecraft.getInstance().displayGuiScreen(null);
			}));
			++id;
		}
	}

	@Override
	public void render(int p_render_1_, int p_render_2_, float p_render_3_) {
		super.render(p_render_1_, p_render_2_, p_render_3_);
	}

	
	
}
