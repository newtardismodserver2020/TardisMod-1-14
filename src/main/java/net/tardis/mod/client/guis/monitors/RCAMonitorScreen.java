package net.tardis.mod.client.guis.monitors;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;

public class RCAMonitorScreen extends BaseMonitorScreen {

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/monitors/rca.png");
	
	
	@Override
	public void renderMonitor() {
		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		this.renderBackground();
		int texWidth = 254, texHeight = 182;
		this.blit(width / 2 - texWidth / 2, height / 2 - texHeight / 2, 0, 0, texWidth, texHeight);
	}

	@Override
	public int getMinY() {
		return this.height / 2 + 63;
	}

	@Override
	public int getMinX() {
		return this.width / 2 - 95;
	}

	@Override
	public int getMaxX() {
		return this.getMinX() + 200;
	}

	@Override
	public int getMaxY() {
		return this.getMinY() - 140;
	}

}
