package net.tardis.mod.client.guis.vm.widgets;


import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;

public class VortexMButtonCatCycleDown extends Button{
	
	ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/vm_ui.png");
	int BUTTON_WIDTH = 12;
	int BUTTON_HEIGHT = 6;

	
	public VortexMButtonCatCycleDown(int x, int y, int width, int height, String buttonText, IPressable onPress) {
		super(x, y, width, height, buttonText, onPress);
	}
	
	@Override
	public void render(int mouseX, int mouseY, float partialTicks) {
		if(active) {
			Minecraft.getInstance().textureManager.bindTexture(TEXTURE);
			boolean isHovered;
			if (mouseX >= x && mouseY >= y && mouseX < x + width && mouseY < y + height) {
				isHovered = true;
			}
			else {
				isHovered = false;
			}
			if (isHovered) {
				blit(this.x, this.y, 45, 232, BUTTON_WIDTH, BUTTON_HEIGHT); //Left Function Cycle Button
			} 
			else {
				blit(this.x, this.y, 17, 232, BUTTON_WIDTH, BUTTON_HEIGHT); //Left Function Cycle Button - Hovered
			}
		}
		
	}
}

