package net.tardis.mod.client.guis;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.gui.widget.Widget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.client.guis.widgets.TextButton;
import net.tardis.mod.misc.Partition;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.TelepathicMessage;

public class TelepathicScreen extends Screen{

	public static final TranslationTextComponent TITLE = new TranslationTextComponent("gui.tardis.telepathic");
	private static final int MAX_ELEM_PER_PAGE = 10;
	private List<Biome> biomeNames = new ArrayList<>(ForgeRegistries.BIOMES.getValues());
    private List<List<Biome>> biomePages = Partition.ofSize(biomeNames, MAX_ELEM_PER_PAGE);
    private int page = 0;
    private TextFieldWidget textFieldWidget = null;
    private String searchTerm = "";
	
	public TelepathicScreen() {
		super(TITLE);
	}

	@Override
    public void init() {
        super.init();
        this.textFieldWidget = new TextFieldWidget(this.font, this.width / 2 - 100, 50, 200, 20, this.textFieldWidget, I18n.format("selectWorld.search"));
        this.children.add(this.textFieldWidget);

        for (Widget b : this.buttons)
            b.active = false;

        this.buttons.clear();

        createFilteredList(searchTerm, true);
        textFieldWidget.setEnabled(true);
    }

    @Override
    public void tick() {
        super.tick();
        textFieldWidget.tick();
    }

    @Override
    public void render(int mouseX, int mouseY, float partialTicks) {
        this.renderBackground();
        for (Widget w : this.buttons) {
            w.renderButton(mouseX, mouseY, partialTicks);
        }
        textFieldWidget.render(mouseX, mouseY, partialTicks);
        
        if (this.textFieldWidget.isFocused()) {
			this.textFieldWidget.setSuggestion("");
		}
		else if (this.textFieldWidget.getText().isEmpty()){
			this.textFieldWidget.setSuggestion("Search...");
		}
    }


    public void createFilteredList(String searchTerm, boolean addBackButtons) {
        this.biomeNames.clear();
        this.biomeNames.addAll(ForgeRegistries.BIOMES.getValues());
        buttons.clear();

        int index = 0;

        if (!searchTerm.isEmpty()) {
            biomeNames.removeIf(biome -> !biome.getDisplayName().getUnformattedComponentText().contains(searchTerm));
        }

        biomePages = Partition.ofSize(biomeNames, MAX_ELEM_PER_PAGE);

        if (!biomeNames.isEmpty()) {
            for (Biome biome : biomePages.get(page)) {
                this.addButton(new TextButton(width / 2 - 50, height / 2 + 50 - (index * font.FONT_HEIGHT + 2), biome.getDisplayName().getFormattedText(), but -> {
                    Network.sendToServer(new TelepathicMessage(biome.getRegistryName()));
                    Minecraft.getInstance().displayGuiScreen(null);
                }));
                ++index;
            }
        }

        if (addBackButtons) {
            this.addButton(new Button(width / 2 + 50, height / 2 + 75, 20, 20, ">", but -> mod(1)));
            this.addButton(new Button(width / 2 - 80, height / 2 + 75, 20, 20, "<", but -> mod(-1)));
        }

    }

	public void mod(int m) {
        int pages = biomePages.size();
        if (page + m >= 0 && page + m < pages)
            page += m;
        init();
	}

    @Override
    public boolean keyReleased(int keyCode, int scanCode, int modifiers) {
        if (textFieldWidget.isFocused()) {
            this.page = 0;
            searchTerm = textFieldWidget.getText();
            createFilteredList(searchTerm, true);
        }
        return super.keyReleased(keyCode, scanCode, modifiers);
    }

    @Override
    public boolean mouseClicked(double p_mouseClicked_1_, double p_mouseClicked_3_, int p_mouseClicked_5_) {
        textFieldWidget.mouseClicked(p_mouseClicked_1_, p_mouseClicked_3_, p_mouseClicked_5_);
        return super.mouseClicked(p_mouseClicked_1_, p_mouseClicked_3_, p_mouseClicked_5_);
    }

    @Override
    public boolean charTyped(char p_charTyped_1_, int p_charTyped_2_) {
        return this.textFieldWidget.charTyped(p_charTyped_1_, p_charTyped_2_);
    }
}
