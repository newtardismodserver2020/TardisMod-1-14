package net.tardis.mod.client.models.exteriors;
//Made with Blockbench
//Paste this code into your mod.

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.tardis.mod.client.models.IBotiModel;
import net.tardis.mod.client.models.IExteriorModel;
import net.tardis.mod.client.renderers.exteriors.SteamExteriorRenderer;
import net.tardis.mod.entity.TardisEntity;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.helper.ModelHelper;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class SteamExteriorModel extends Model implements IBotiModel, IExteriorModel{
	private final RendererModel glow;
	private final RendererModel glow_toplamp_glass;
	private final RendererModel glow_front_window;
	private final RendererModel glow_roof_windows_front;
	private final RendererModel glow_side_1;
	private final RendererModel glow_side_window_1;
	private final RendererModel glow_roof_windows_side_1;
	private final RendererModel glow_side_2;
	private final RendererModel glow_side_window_2;
	private final RendererModel glow_roof_windows_side_2;
	private final RendererModel glow_side_3;
	private final RendererModel glow_side_window_3;
	private final RendererModel glow_roof_windows_side_3;
	private final RendererModel glow_side_4;
	private final RendererModel glow_side_window_4;
	private final RendererModel glow_roof_windows_side_4;
	private final RendererModel glow_side_5;
	private final RendererModel glow_side_window_5;
	private final RendererModel glow_roof_windows_side_5;
	private final RendererModel door_rotate_y;
	private final RendererModel handle;
	private final RendererModel lock;
	private final RendererModel kickplate;
	private final RendererModel hinge;
	private final RendererModel boti;
	private final RendererModel box;
	private final RendererModel Lamp;
	private final RendererModel lamp_ribs_1;
	private final RendererModel lamp_ribs_2;
	private final RendererModel lamp_ribs_3;
	private final RendererModel lamp_base;
	private final RendererModel lamp_cap;
	private final RendererModel panel_front;
	private final RendererModel floorboards;
	private final RendererModel roof_front;
	private final RendererModel center_peak;
	private final RendererModel wroughtiron_front;
	private final RendererModel outer_peak;
	private final RendererModel panel_front_top;
	private final RendererModel panel_side_1;
	private final RendererModel side_1;
	private final RendererModel top_panel_1;
	private final RendererModel cage;
	private final RendererModel middle_panel_1;
	private final RendererModel lower_panel_1;
	private final RendererModel componets;
	private final RendererModel panelbox;
	private final RendererModel panelbox2;
	private final RendererModel floorboards_1;
	private final RendererModel roof_side_1;
	private final RendererModel center_peak_1;
	private final RendererModel wroughtiron_front_1;
	private final RendererModel outer_peak_1;
	private final RendererModel panel_side_2;
	private final RendererModel side_2;
	private final RendererModel top_panel_2;
	private final RendererModel cage2;
	private final RendererModel middle_panel_2;
	private final RendererModel lower_panel_2;
	private final RendererModel componets2;
	private final RendererModel panelbox3;
	private final RendererModel panelbox4;
	private final RendererModel floorboards_2;
	private final RendererModel roof_side_2;
	private final RendererModel center_peak_2;
	private final RendererModel wroughtiron_front_2;
	private final RendererModel outer_peak_2;
	private final RendererModel panel_side_3;
	private final RendererModel roof_side_3;
	private final RendererModel center_peak_3;
	private final RendererModel wroughtiron_front_3;
	private final RendererModel outer_peak_3;
	private final RendererModel side_3;
	private final RendererModel top_panel_3;
	private final RendererModel cage3;
	private final RendererModel middle_panel_3;
	private final RendererModel lower_panel_3;
	private final RendererModel componets3;
	private final RendererModel gear;
	private final RendererModel gear2;
	private final RendererModel gear3;
	private final RendererModel panelbox5;
	private final RendererModel panelbox6;
	private final RendererModel floorboards_3;
	private final RendererModel panel_side_4;
	private final RendererModel roof_side_4;
	private final RendererModel center_peak_4;
	private final RendererModel wroughtiron_front_4;
	private final RendererModel outer_peak_4;
	private final RendererModel side_4;
	private final RendererModel top_panel_4;
	private final RendererModel cage4;
	private final RendererModel middle_panel_4;
	private final RendererModel lower_panel_4;
	private final RendererModel gear4;
	private final RendererModel componets4;
	private final RendererModel panelbox7;
	private final RendererModel panelbox8;
	private final RendererModel floorboards_4;
	private final RendererModel panel_side_5;
	private final RendererModel roof_side_5;
	private final RendererModel center_peak_5;
	private final RendererModel wroughtiron_front_5;
	private final RendererModel outer_peak_5;
	private final RendererModel side_5;
	private final RendererModel top_panel_5;
	private final RendererModel cage5;
	private final RendererModel middle_panel_5;
	private final RendererModel lower_panel_5;
	private final RendererModel componets5;
	private final RendererModel panelbox9;
	private final RendererModel panelbox10;
	private final RendererModel floorboards_5;
	private final RendererModel hexspine;
	private final RendererModel crossspine;
	private final RendererModel leftspine;
	private final RendererModel upperrails;
	private final RendererModel upperrail_1;
	private final RendererModel upperrails2;
	private final RendererModel upperrail_2;
	private final RendererModel upperrails3;
	private final RendererModel upperrail_3;
	private final RendererModel upperrails4;
	private final RendererModel upperrail_4;
	private final RendererModel upperrails5;
	private final RendererModel upperrail_5;
	private final RendererModel upperrails6;
	private final RendererModel upperrail_6;
	private final RendererModel rightspine;

	public SteamExteriorModel() {
		textureWidth = 1024;
		textureHeight = 1024;

		glow = new RendererModel(this);
		glow.setRotationPoint(0.0F, 24.0F, 0.0F);

		glow_toplamp_glass = new RendererModel(this);
		glow_toplamp_glass.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow.addChild(glow_toplamp_glass);
		glow_toplamp_glass.cubeList.add(new ModelBox(glow_toplamp_glass, 512, 146, -4.0F, -240.0F, -6.0F, 8, 20, 4, 0.0F, false));
		glow_toplamp_glass.cubeList.add(new ModelBox(glow_toplamp_glass, 512, 146, -4.0F, -240.0F, 6.0F, 8, 20, 4, 0.0F, false));
		glow_toplamp_glass.cubeList.add(new ModelBox(glow_toplamp_glass, 512, 146, 4.0F, -240.0F, -2.0F, 4, 20, 8, 0.0F, false));
		glow_toplamp_glass.cubeList.add(new ModelBox(glow_toplamp_glass, 512, 146, -8.0F, -240.0F, -2.0F, 4, 20, 8, 0.0F, false));

		glow_front_window = new RendererModel(this);
		glow_front_window.setRotationPoint(0.0F, -3.7778F, 1.6667F);
		glow.addChild(glow_front_window);
		glow_front_window.cubeList.add(new ModelBox(glow_front_window, 525, 162, 6.0F, -176.2222F, -51.8667F, 12, 12, 8, 0.0F, false));
		glow_front_window.cubeList.add(new ModelBox(glow_front_window, 507, 157, -18.0F, -176.2222F, -51.8667F, 12, 12, 8, 0.0F, false));

		glow_roof_windows_front = new RendererModel(this);
		glow_roof_windows_front.setRotationPoint(0.0F, -191.7778F, -30.3333F);
		glow.addChild(glow_roof_windows_front);
		glow_roof_windows_front.cubeList.add(new ModelBox(glow_roof_windows_front, 464, 150, -18.6F, -12.2222F, -0.6667F, 36, 8, 4, 0.0F, false));

		glow_side_1 = new RendererModel(this);
		glow_side_1.setRotationPoint(0.0F, 0.0F, 2.0F);
		setRotationAngle(glow_side_1, 0.0F, -1.0472F, 0.0F);
		glow.addChild(glow_side_1);

		glow_side_window_1 = new RendererModel(this);
		glow_side_window_1.setRotationPoint(0.0F, -160.0F, -55.5F);
		glow_side_1.addChild(glow_side_window_1);
		glow_side_window_1.cubeList.add(new ModelBox(glow_side_window_1, 505, 137, -12.0F, -12.0F, -3.5F, 24, 24, 16, 0.0F, false));
		glow_side_window_1.cubeList.add(new ModelBox(glow_side_window_1, 505, 137, -10.0F, -15.0F, -1.5F, 20, 4, 14, 0.0F, false));
		glow_side_window_1.cubeList.add(new ModelBox(glow_side_window_1, 505, 137, -10.0F, 11.0F, -1.5F, 20, 4, 14, 0.0F, false));
		glow_side_window_1.cubeList.add(new ModelBox(glow_side_window_1, 505, 137, 11.0F, -10.0F, -1.5F, 4, 20, 14, 0.0F, false));
		glow_side_window_1.cubeList.add(new ModelBox(glow_side_window_1, 505, 137, -15.0F, -10.0F, -1.5F, 4, 20, 14, 0.0F, false));

		glow_roof_windows_side_1 = new RendererModel(this);
		glow_roof_windows_side_1.setRotationPoint(0.0F, -191.7778F, -32.3333F);
		glow_side_1.addChild(glow_roof_windows_side_1);
		glow_roof_windows_side_1.cubeList.add(new ModelBox(glow_roof_windows_side_1, 505, 137, -19.0F, -12.2222F, -0.6667F, 36, 8, 4, 0.0F, false));

		glow_side_2 = new RendererModel(this);
		glow_side_2.setRotationPoint(0.0F, 0.0F, 2.0F);
		setRotationAngle(glow_side_2, 0.0F, -2.0944F, 0.0F);
		glow.addChild(glow_side_2);

		glow_side_window_2 = new RendererModel(this);
		glow_side_window_2.setRotationPoint(0.0F, -160.0F, -55.5F);
		glow_side_2.addChild(glow_side_window_2);
		glow_side_window_2.cubeList.add(new ModelBox(glow_side_window_2, 502, 130, -12.0F, -12.0F, -3.5F, 24, 24, 16, 0.0F, false));
		glow_side_window_2.cubeList.add(new ModelBox(glow_side_window_2, 502, 130, -10.0F, -15.0F, -1.5F, 20, 4, 14, 0.0F, false));
		glow_side_window_2.cubeList.add(new ModelBox(glow_side_window_2, 502, 130, -10.0F, 11.0F, -1.5F, 20, 4, 14, 0.0F, false));
		glow_side_window_2.cubeList.add(new ModelBox(glow_side_window_2, 502, 130, 11.0F, -10.0F, -1.5F, 4, 20, 14, 0.0F, false));
		glow_side_window_2.cubeList.add(new ModelBox(glow_side_window_2, 502, 130, -15.0F, -10.0F, -1.5F, 4, 20, 14, 0.0F, false));

		glow_roof_windows_side_2 = new RendererModel(this);
		glow_roof_windows_side_2.setRotationPoint(0.0F, -191.7778F, -32.3333F);
		glow_side_2.addChild(glow_roof_windows_side_2);
		glow_roof_windows_side_2.cubeList.add(new ModelBox(glow_roof_windows_side_2, 509, 132, -19.0F, -12.2222F, -0.6667F, 36, 8, 4, 0.0F, false));

		glow_side_3 = new RendererModel(this);
		glow_side_3.setRotationPoint(0.0F, 0.0F, 2.0F);
		setRotationAngle(glow_side_3, 0.0F, 3.1416F, 0.0F);
		glow.addChild(glow_side_3);

		glow_side_window_3 = new RendererModel(this);
		glow_side_window_3.setRotationPoint(0.0F, -160.0F, -55.5F);
		glow_side_3.addChild(glow_side_window_3);
		glow_side_window_3.cubeList.add(new ModelBox(glow_side_window_3, 496, 130, -12.0F, -12.0F, -3.5F, 24, 24, 16, 0.0F, false));
		glow_side_window_3.cubeList.add(new ModelBox(glow_side_window_3, 496, 130, -10.0F, -15.0F, -1.5F, 20, 4, 14, 0.0F, false));
		glow_side_window_3.cubeList.add(new ModelBox(glow_side_window_3, 496, 130, -10.0F, 11.0F, -1.5F, 20, 4, 14, 0.0F, false));
		glow_side_window_3.cubeList.add(new ModelBox(glow_side_window_3, 496, 130, 11.0F, -10.0F, -1.5F, 4, 20, 14, 0.0F, false));
		glow_side_window_3.cubeList.add(new ModelBox(glow_side_window_3, 496, 130, -15.0F, -10.0F, -1.5F, 4, 20, 14, 0.0F, false));

		glow_roof_windows_side_3 = new RendererModel(this);
		glow_roof_windows_side_3.setRotationPoint(0.0F, -191.7778F, -32.3333F);
		glow_side_3.addChild(glow_roof_windows_side_3);
		glow_roof_windows_side_3.cubeList.add(new ModelBox(glow_roof_windows_side_3, 496, 130, -19.0F, -12.2222F, -0.6667F, 36, 8, 4, 0.0F, false));

		glow_side_4 = new RendererModel(this);
		glow_side_4.setRotationPoint(0.0F, 0.0F, 2.0F);
		setRotationAngle(glow_side_4, 0.0F, 2.0944F, 0.0F);
		glow.addChild(glow_side_4);

		glow_side_window_4 = new RendererModel(this);
		glow_side_window_4.setRotationPoint(0.0F, -160.0F, -55.5F);
		glow_side_4.addChild(glow_side_window_4);
		glow_side_window_4.cubeList.add(new ModelBox(glow_side_window_4, 507, 134, -12.0F, -12.0F, -3.5F, 24, 24, 16, 0.0F, false));
		glow_side_window_4.cubeList.add(new ModelBox(glow_side_window_4, 507, 134, -10.0F, -15.0F, -1.5F, 20, 4, 14, 0.0F, false));
		glow_side_window_4.cubeList.add(new ModelBox(glow_side_window_4, 507, 134, -10.0F, 11.0F, -1.5F, 20, 4, 14, 0.0F, false));
		glow_side_window_4.cubeList.add(new ModelBox(glow_side_window_4, 507, 134, 11.0F, -10.0F, -1.5F, 4, 20, 14, 0.0F, false));
		glow_side_window_4.cubeList.add(new ModelBox(glow_side_window_4, 507, 134, -15.0F, -10.0F, -1.5F, 4, 20, 14, 0.0F, false));

		glow_roof_windows_side_4 = new RendererModel(this);
		glow_roof_windows_side_4.setRotationPoint(0.0F, -191.7778F, -32.3333F);
		glow_side_4.addChild(glow_roof_windows_side_4);
		glow_roof_windows_side_4.cubeList.add(new ModelBox(glow_roof_windows_side_4, 507, 134, -19.0F, -12.2222F, -0.6667F, 36, 8, 4, 0.0F, false));

		glow_side_5 = new RendererModel(this);
		glow_side_5.setRotationPoint(0.0F, 0.0F, 2.0F);
		setRotationAngle(glow_side_5, 0.0F, 1.0472F, 0.0F);
		glow.addChild(glow_side_5);

		glow_side_window_5 = new RendererModel(this);
		glow_side_window_5.setRotationPoint(0.0F, -160.0F, -55.5F);
		glow_side_5.addChild(glow_side_window_5);
		glow_side_window_5.cubeList.add(new ModelBox(glow_side_window_5, 500, 132, -12.0F, -12.0F, -3.5F, 24, 24, 16, 0.0F, false));
		glow_side_window_5.cubeList.add(new ModelBox(glow_side_window_5, 500, 132, -10.0F, -15.0F, -1.5F, 20, 4, 14, 0.0F, false));
		glow_side_window_5.cubeList.add(new ModelBox(glow_side_window_5, 500, 132, -10.0F, 11.0F, -1.5F, 20, 4, 14, 0.0F, false));
		glow_side_window_5.cubeList.add(new ModelBox(glow_side_window_5, 500, 132, 11.0F, -10.0F, -1.5F, 4, 20, 14, 0.0F, false));
		glow_side_window_5.cubeList.add(new ModelBox(glow_side_window_5, 500, 132, -15.0F, -10.0F, -1.5F, 4, 20, 14, 0.0F, false));

		glow_roof_windows_side_5 = new RendererModel(this);
		glow_roof_windows_side_5.setRotationPoint(0.0F, -191.7778F, -32.3333F);
		glow_side_5.addChild(glow_roof_windows_side_5);
		glow_roof_windows_side_5.cubeList.add(new ModelBox(glow_roof_windows_side_5, 500, 132, -19.0F, -12.2222F, -0.6667F, 36, 8, 4, 0.0F, false));

		door_rotate_y = new RendererModel(this);
		door_rotate_y.setRotationPoint(28.0F, -22.0F, -44.0F);
		door_rotate_y.cubeList.add(new ModelBox(door_rotate_y, 171, 374, -56.0F, 14.0F, -4.0F, 56, 20, 4, 0.0F, false));
		door_rotate_y.cubeList.add(new ModelBox(door_rotate_y, 242, 25, -56.0F, -106.0F, -3.0F, 56, 140, 2, 0.0F, false));
		door_rotate_y.cubeList.add(new ModelBox(door_rotate_y, 162, 258, -56.0F, -106.0F, -4.0F, 56, 8, 4, 0.0F, false));
		door_rotate_y.cubeList.add(new ModelBox(door_rotate_y, 128, 274, -12.0F, -98.0F, -4.0F, 12, 112, 4, 0.0F, false));
		door_rotate_y.cubeList.add(new ModelBox(door_rotate_y, 173, 285, -56.0F, -98.0F, -4.0F, 12, 112, 4, 0.0F, false));
		door_rotate_y.cubeList.add(new ModelBox(door_rotate_y, 171, 262, -42.0F, -96.0F, -4.0F, 28, 4, 4, 0.0F, false));
		door_rotate_y.cubeList.add(new ModelBox(door_rotate_y, 173, 379, -42.0F, 4.0F, -4.0F, 28, 8, 4, 0.0F, false));
		door_rotate_y.cubeList.add(new ModelBox(door_rotate_y, 173, 285, -42.0F, -92.0F, -4.0F, 4, 96, 4, 0.0F, false));
		door_rotate_y.cubeList.add(new ModelBox(door_rotate_y, 173, 285, -18.0F, -92.0F, -4.0F, 4, 96, 4, 0.0F, false));
		door_rotate_y.cubeList.add(new ModelBox(door_rotate_y, 173, 269, -36.0F, -90.0F, -4.0F, 16, 16, 4, 0.0F, false));
		door_rotate_y.cubeList.add(new ModelBox(door_rotate_y, 182, 372, -36.0F, -14.0F, -4.0F, 16, 16, 4, 0.0F, false));
		door_rotate_y.cubeList.add(new ModelBox(door_rotate_y, 171, 269, -36.0F, -72.0F, -4.0F, 16, 56, 4, 0.0F, false));

		handle = new RendererModel(this);
		handle.setRotationPoint(-4.0F, 34.0F, 0.0F);
		door_rotate_y.addChild(handle);
		handle.cubeList.add(new ModelBox(handle, 509, 43, -46.0F, -86.0F, -5.0F, 4, 12, 6, 0.0F, false));

		lock = new RendererModel(this);
		lock.setRotationPoint(-44.0F, -69.8F, -3.8F);
		setRotationAngle(lock, 0.0873F, 0.0873F, -0.7854F);
		handle.addChild(lock);
		lock.cubeList.add(new ModelBox(lock, 509, 43, -2.0F, -2.0F, -1.0F, 4, 4, 2, 0.0F, false));

		kickplate = new RendererModel(this);
		kickplate.setRotationPoint(-4.0F, 34.0F, 0.0F);
		door_rotate_y.addChild(kickplate);
		kickplate.cubeList.add(new ModelBox(kickplate, 466, 36, -46.0F, -18.0F, -5.0F, 44, 16, 4, 0.0F, false));
		kickplate.cubeList.add(new ModelBox(kickplate, 466, 36, -45.0F, -17.0F, -5.8F, 2, 2, 4, 0.0F, false));
		kickplate.cubeList.add(new ModelBox(kickplate, 466, 36, -45.0F, -5.0F, -5.8F, 2, 2, 4, 0.0F, false));
		kickplate.cubeList.add(new ModelBox(kickplate, 466, 36, -5.0F, -5.0F, -5.8F, 2, 2, 4, 0.0F, false));
		kickplate.cubeList.add(new ModelBox(kickplate, 466, 36, -5.0F, -17.0F, -5.8F, 2, 2, 4, 0.0F, false));

		hinge = new RendererModel(this);
		hinge.setRotationPoint(0.0F, -44.0F, -0.2F);
		setRotationAngle(hinge, 0.0F, -0.6981F, 0.0F);
		door_rotate_y.addChild(hinge);
		hinge.cubeList.add(new ModelBox(hinge, 497, 74, -2.0F, -54.0F, -2.0F, 4, 12, 4, 0.0F, false));
		hinge.cubeList.add(new ModelBox(hinge, 497, 74, -2.0F, -8.0F, -2.0F, 4, 12, 4, 0.0F, false));
		hinge.cubeList.add(new ModelBox(hinge, 497, 74, -2.0F, 44.0F, -2.0F, 4, 12, 4, 0.0F, false));

		boti = new RendererModel(this);
		boti.setRotationPoint(0.0F, 24.0F, 0.0F);
		boti.cubeList.add(new ModelBox(boti, 0, 0, -31.0F, -196.0F, -43.0F, 61, 192, 0, 0.0F, false));

		box = new RendererModel(this);
		box.setRotationPoint(0.0F, 24.0F, 0.0F);

		Lamp = new RendererModel(this);
		Lamp.setRotationPoint(0.0F, 0.0F, 0.0F);
		box.addChild(Lamp);

		lamp_ribs_1 = new RendererModel(this);
		lamp_ribs_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		Lamp.addChild(lamp_ribs_1);
		lamp_ribs_1.cubeList.add(new ModelBox(lamp_ribs_1, 697, 50, -6.0F, -225.0F, -7.0F, 12, 2, 4, 0.0F, false));
		lamp_ribs_1.cubeList.add(new ModelBox(lamp_ribs_1, 697, 50, -6.0F, -225.0F, 7.0F, 12, 2, 4, 0.0F, false));
		lamp_ribs_1.cubeList.add(new ModelBox(lamp_ribs_1, 697, 50, 1.0F, -225.0F, -4.0F, 8, 2, 12, 0.0F, false));
		lamp_ribs_1.cubeList.add(new ModelBox(lamp_ribs_1, 697, 50, -9.0F, -225.0F, -4.0F, 8, 2, 12, 0.0F, false));

		lamp_ribs_2 = new RendererModel(this);
		lamp_ribs_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		Lamp.addChild(lamp_ribs_2);
		lamp_ribs_2.cubeList.add(new ModelBox(lamp_ribs_2, 681, 54, -6.0F, -230.0F, -7.0F, 12, 2, 4, 0.0F, false));
		lamp_ribs_2.cubeList.add(new ModelBox(lamp_ribs_2, 681, 54, -6.0F, -230.0F, 7.0F, 12, 2, 4, 0.0F, false));
		lamp_ribs_2.cubeList.add(new ModelBox(lamp_ribs_2, 681, 54, 1.0F, -230.0F, -4.0F, 8, 2, 12, 0.0F, false));
		lamp_ribs_2.cubeList.add(new ModelBox(lamp_ribs_2, 681, 54, -9.0F, -230.0F, -4.0F, 8, 2, 12, 0.0F, false));

		lamp_ribs_3 = new RendererModel(this);
		lamp_ribs_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		Lamp.addChild(lamp_ribs_3);
		lamp_ribs_3.cubeList.add(new ModelBox(lamp_ribs_3, 688, 61, -6.0F, -235.0F, -7.0F, 12, 2, 4, 0.0F, false));
		lamp_ribs_3.cubeList.add(new ModelBox(lamp_ribs_3, 688, 61, -6.0F, -235.0F, 7.0F, 12, 2, 4, 0.0F, false));
		lamp_ribs_3.cubeList.add(new ModelBox(lamp_ribs_3, 688, 61, 1.0F, -235.0F, -4.0F, 8, 2, 12, 0.0F, false));
		lamp_ribs_3.cubeList.add(new ModelBox(lamp_ribs_3, 688, 61, -9.0F, -235.0F, -4.0F, 8, 2, 12, 0.0F, false));

		lamp_base = new RendererModel(this);
		lamp_base.setRotationPoint(0.0F, 0.0F, 0.0F);
		Lamp.addChild(lamp_base);
		lamp_base.cubeList.add(new ModelBox(lamp_base, 701, 70, -6.0F, -220.0F, -7.0F, 12, 4, 4, 0.0F, false));
		lamp_base.cubeList.add(new ModelBox(lamp_base, 701, 70, -6.0F, -220.0F, 7.0F, 12, 4, 4, 0.0F, false));
		lamp_base.cubeList.add(new ModelBox(lamp_base, 701, 70, 1.0F, -220.0F, -4.0F, 8, 4, 12, 0.0F, false));
		lamp_base.cubeList.add(new ModelBox(lamp_base, 701, 70, -9.0F, -220.0F, -4.0F, 8, 4, 12, 0.0F, false));

		lamp_cap = new RendererModel(this);
		lamp_cap.setRotationPoint(0.0F, 0.0F, 0.0F);
		Lamp.addChild(lamp_cap);
		lamp_cap.cubeList.add(new ModelBox(lamp_cap, 697, 41, -6.0F, -242.0F, -7.0F, 12, 4, 8, 0.0F, false));
		lamp_cap.cubeList.add(new ModelBox(lamp_cap, 697, 41, -6.0F, -242.0F, 3.0F, 12, 4, 8, 0.0F, false));
		lamp_cap.cubeList.add(new ModelBox(lamp_cap, 697, 41, 1.0F, -242.0F, -4.0F, 8, 4, 12, 0.0F, false));
		lamp_cap.cubeList.add(new ModelBox(lamp_cap, 697, 41, -9.0F, -242.0F, -4.0F, 8, 4, 12, 0.0F, false));
		lamp_cap.cubeList.add(new ModelBox(lamp_cap, 697, 41, -6.0F, -244.0F, -4.0F, 12, 4, 12, 0.0F, false));
		lamp_cap.cubeList.add(new ModelBox(lamp_cap, 697, 41, -2.0F, -245.2F, 0.0F, 4, 4, 4, 0.0F, false));

		panel_front = new RendererModel(this);
		panel_front.setRotationPoint(0.0F, -3.7778F, 1.6667F);
		box.addChild(panel_front);
		panel_front.cubeList.add(new ModelBox(panel_front, 160, 392, -28.0F, -8.2222F, -50.6667F, 56, 8, 4, 0.0F, false));
		panel_front.cubeList.add(new ModelBox(panel_front, 148, 220, -28.0F, -180.2222F, -50.6667F, 4, 172, 4, 0.0F, false));
		panel_front.cubeList.add(new ModelBox(panel_front, 288, 226, 24.0F, -180.2222F, -50.6667F, 4, 172, 4, 0.0F, false));
		panel_front.cubeList.add(new ModelBox(panel_front, 189, 253, -28.0F, -188.2222F, -50.6667F, 56, 8, 4, 0.0F, false));
		panel_front.cubeList.add(new ModelBox(panel_front, 98, 299, -24.0F, -160.2222F, -50.6667F, 48, 12, 4, 0.0F, false));
		panel_front.cubeList.add(new ModelBox(panel_front, 180, 285, -28.0F, -191.4222F, -52.6667F, 56, 4, 4, 0.0F, false));
		panel_front.cubeList.add(new ModelBox(panel_front, 445, 54, -28.4F, -156.2222F, -52.2667F, 56, 4, 6, 0.0F, false));

		floorboards = new RendererModel(this);
		floorboards.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_front.addChild(floorboards);
		floorboards.cubeList.add(new ModelBox(floorboards, 237, 185, -32.0F, -0.2222F, -58.6667F, 64, 4, 4, 0.0F, false));
		floorboards.cubeList.add(new ModelBox(floorboards, 237, 185, -26.0F, -0.2222F, -46.6667F, 52, 4, 4, 0.0F, false));
		floorboards.cubeList.add(new ModelBox(floorboards, 237, 185, -24.0F, -0.2222F, -42.6667F, 48, 4, 4, 0.0F, false));
		floorboards.cubeList.add(new ModelBox(floorboards, 237, 185, -22.0F, -0.2222F, -38.6667F, 44, 4, 4, 0.0F, false));
		floorboards.cubeList.add(new ModelBox(floorboards, 237, 185, -18.0F, -0.2222F, -34.6667F, 36, 4, 4, 0.0F, false));
		floorboards.cubeList.add(new ModelBox(floorboards, 237, 185, -16.0F, -0.2222F, -30.6667F, 32, 4, 4, 0.0F, false));
		floorboards.cubeList.add(new ModelBox(floorboards, 237, 185, -14.0F, -0.2222F, -26.6667F, 28, 4, 4, 0.0F, false));
		floorboards.cubeList.add(new ModelBox(floorboards, 237, 185, -12.0F, -0.2222F, -22.6667F, 24, 4, 4, 0.0F, false));
		floorboards.cubeList.add(new ModelBox(floorboards, 237, 185, -10.0F, -0.2222F, -18.6667F, 20, 4, 4, 0.0F, false));
		floorboards.cubeList.add(new ModelBox(floorboards, 237, 185, -8.0F, -0.2222F, -14.6667F, 16, 4, 4, 0.0F, false));
		floorboards.cubeList.add(new ModelBox(floorboards, 237, 185, -6.0F, -0.2222F, -10.6667F, 12, 4, 4, 0.0F, false));
		floorboards.cubeList.add(new ModelBox(floorboards, 237, 185, -4.0F, -0.2222F, -6.6667F, 8, 4, 4, 0.0F, false));
		floorboards.cubeList.add(new ModelBox(floorboards, 237, 185, -30.0F, -0.2222F, -54.6667F, 60, 4, 4, 0.0F, false));
		floorboards.cubeList.add(new ModelBox(floorboards, 237, 185, -28.0F, -0.2222F, -50.6667F, 56, 4, 4, 0.0F, false));

		roof_front = new RendererModel(this);
		roof_front.setRotationPoint(0.0F, -188.0F, -48.0F);
		panel_front.addChild(roof_front);

		center_peak = new RendererModel(this);
		center_peak.setRotationPoint(0.0F, 0.0F, 16.0F);
		setRotationAngle(center_peak, 0.1047F, 0.0F, 0.0F);
		roof_front.addChild(center_peak);
		center_peak.cubeList.add(new ModelBox(center_peak, 182, 299, -18.0F, -13.8222F, -1.4667F, 37, 2, 4, 0.0F, false));
		center_peak.cubeList.add(new ModelBox(center_peak, 182, 299, -6.0F, -18.2222F, 22.5333F, 12, 8, 4, 0.0F, false));
		center_peak.cubeList.add(new ModelBox(center_peak, 182, 299, -12.0F, -12.2222F, -1.0667F, 4, 8, 8, 0.0F, false));
		center_peak.cubeList.add(new ModelBox(center_peak, 182, 299, -2.0F, -12.2222F, -1.0667F, 4, 8, 8, 0.0F, false));
		center_peak.cubeList.add(new ModelBox(center_peak, 182, 299, 8.0F, -12.2222F, -1.0667F, 4, 8, 8, 0.0F, false));
		center_peak.cubeList.add(new ModelBox(center_peak, 180, 292, -16.0F, -13.8222F, 2.5333F, 32, 2, 4, 0.0F, false));
		center_peak.cubeList.add(new ModelBox(center_peak, 173, 276, -14.0F, -13.8222F, 6.5333F, 28, 2, 4, 0.0F, false));
		center_peak.cubeList.add(new ModelBox(center_peak, 180, 292, -12.0F, -13.8222F, 10.5333F, 24, 2, 4, 0.0F, false));
		center_peak.cubeList.add(new ModelBox(center_peak, 187, 278, -10.0F, -13.8222F, 14.5333F, 20, 2, 4, 0.0F, false));
		center_peak.cubeList.add(new ModelBox(center_peak, 185, 272, -8.0F, -13.8222F, 18.5333F, 16, 2, 4, 0.0F, false));

		wroughtiron_front = new RendererModel(this);
		wroughtiron_front.setRotationPoint(0.0F, 191.7778F, 46.3333F);
		roof_front.addChild(wroughtiron_front);
		wroughtiron_front.cubeList.add(new ModelBox(wroughtiron_front, 6, 420, -32.0F, -220.0F, -54.0F, 64, 24, 0, 0.0F, false));

		outer_peak = new RendererModel(this);
		outer_peak.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(outer_peak, 0.0873F, 0.0F, 0.0F);
		roof_front.addChild(outer_peak);
		outer_peak.cubeList.add(new ModelBox(outer_peak, 182, 299, -32.0F, -5.0222F, -10.6667F, 64, 2, 7, 0.0F, false));
		outer_peak.cubeList.add(new ModelBox(outer_peak, 182, 299, -29.8F, -3.0222F, -7.2667F, 60, 2, 8, 0.0F, false));
		outer_peak.cubeList.add(new ModelBox(outer_peak, 182, 299, -25.6F, -5.0222F, 0.3333F, 52, 4, 4, 0.0F, false));
		outer_peak.cubeList.add(new ModelBox(outer_peak, 134, 299, -23.6F, -5.0222F, 4.3333F, 48, 4, 4, 0.0F, false));
		outer_peak.cubeList.add(new ModelBox(outer_peak, 196, 285, -21.6F, -5.0222F, 8.3333F, 44, 4, 4, 0.0F, false));
		outer_peak.cubeList.add(new ModelBox(outer_peak, 182, 299, -19.6F, -5.0222F, 12.3333F, 40, 4, 4, 0.0F, false));
		outer_peak.cubeList.add(new ModelBox(outer_peak, 182, 299, -4.0F, -24.0222F, 42.3333F, 8, 20, 4, 0.0F, false));
		outer_peak.cubeList.add(new ModelBox(outer_peak, 196, 290, -27.6F, -5.0222F, -3.6667F, 56, 4, 4, 0.0F, false));

		panel_front_top = new RendererModel(this);
		panel_front_top.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_front.addChild(panel_front_top);
		panel_front_top.cubeList.add(new ModelBox(panel_front_top, 274, 73, -24.0F, -180.2222F, -49.6667F, 48, 24, 4, 0.0F, false));
		panel_front_top.cubeList.add(new ModelBox(panel_front_top, 464, 38, 5.0F, -177.2222F, -51.0667F, 14, 14, 6, 0.0F, false));
		panel_front_top.cubeList.add(new ModelBox(panel_front_top, 464, 38, -19.0F, -177.2222F, -51.0667F, 14, 14, 6, 0.0F, false));

		panel_side_1 = new RendererModel(this);
		panel_side_1.setRotationPoint(0.0F, -3.7778F, 1.6667F);
		setRotationAngle(panel_side_1, 0.0F, -1.0472F, 0.0F);
		box.addChild(panel_side_1);

		side_1 = new RendererModel(this);
		side_1.setRotationPoint(0.0F, 3.7778F, -1.6667F);
		panel_side_1.addChild(side_1);
		side_1.cubeList.add(new ModelBox(side_1, 146, 393, -28.0F, -36.0F, -49.0F, 56, 4, 4, 0.0F, false));
		side_1.cubeList.add(new ModelBox(side_1, 139, 249, -28.0F, -184.0F, -49.0F, 4, 148, 4, 0.0F, false));
		side_1.cubeList.add(new ModelBox(side_1, 144, 246, 24.0F, -184.0F, -49.0F, 4, 148, 4, 0.0F, false));
		side_1.cubeList.add(new ModelBox(side_1, 235, 246, -28.0F, -192.0F, -49.0F, 56, 8, 4, 0.0F, false));
		side_1.cubeList.add(new ModelBox(side_1, 171, 352, -24.0F, -136.0F, -49.0F, 48, 12, 4, 0.0F, false));
		side_1.cubeList.add(new ModelBox(side_1, 278, 374, -24.0F, -80.0F, -49.0F, 48, 20, 4, 0.0F, false));
		side_1.cubeList.add(new ModelBox(side_1, 118, 267, -28.0F, -195.2F, -51.0F, 56, 4, 4, 0.0F, false));
		side_1.cubeList.add(new ModelBox(side_1, 452, 18, -28.0F, -132.0F, -50.6F, 56, 4, 6, 0.0F, false));
		side_1.cubeList.add(new ModelBox(side_1, 448, 13, -28.0F, -76.0F, -50.6F, 56, 4, 6, 0.0F, false));
		side_1.cubeList.add(new ModelBox(side_1, 448, 84, -28.0F, -66.0F, -50.6F, 56, 4, 6, 0.0F, false));
		side_1.cubeList.add(new ModelBox(side_1, 459, 6, -28.0F, -33.0F, -50.6F, 56, 4, 6, 0.0F, false));
		side_1.cubeList.add(new ModelBox(side_1, 441, 93, -28.0F, -16.0F, -50.6F, 56, 4, 6, 0.0F, false));
		side_1.cubeList.add(new ModelBox(side_1, 288, 422, -28.0F, -32.0F, -49.0F, 56, 16, 4, 0.0F, false));
		side_1.cubeList.add(new ModelBox(side_1, 140, 392, -28.0F, -12.0F, -49.0F, 56, 8, 4, 0.0F, false));

		top_panel_1 = new RendererModel(this);
		top_panel_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_1.addChild(top_panel_1);
		top_panel_1.cubeList.add(new ModelBox(top_panel_1, 269, 64, -24.0F, -180.2222F, -49.6667F, 48, 48, 4, 0.0F, false));

		cage = new RendererModel(this);
		cage.setRotationPoint(0.0F, 3.7778F, -1.6667F);
		top_panel_1.addChild(cage);
		cage.cubeList.add(new ModelBox(cage, 480, 38, -9.0F, -177.0F, -58.0F, 4, 12, 17, 0.0F, false));
		cage.cubeList.add(new ModelBox(cage, 480, 38, 5.0F, -177.0F, -58.0F, 4, 8, 17, 0.0F, false));
		cage.cubeList.add(new ModelBox(cage, 480, 38, -17.0F, -169.0F, -58.6F, 11, 4, 18, 0.0F, false));
		cage.cubeList.add(new ModelBox(cage, 480, 38, -17.0F, -155.0F, -58.6F, 11, 4, 18, 0.0F, false));
		cage.cubeList.add(new ModelBox(cage, 480, 38, 5.0F, -151.0F, -58.0F, 4, 8, 17, 0.0F, false));
		cage.cubeList.add(new ModelBox(cage, 480, 38, -9.0F, -151.0F, -58.0F, 4, 8, 17, 0.0F, false));
		cage.cubeList.add(new ModelBox(cage, 480, 38, 6.0F, -155.0F, -58.6F, 11, 4, 18, 0.0F, false));
		cage.cubeList.add(new ModelBox(cage, 480, 38, 6.0F, -169.0F, -58.6F, 11, 4, 18, 0.0F, false));
		cage.cubeList.add(new ModelBox(cage, 480, 38, -6.0F, -169.0F, -59.6F, 12, 4, 19, 0.0F, false));
		cage.cubeList.add(new ModelBox(cage, 480, 38, -6.0F, -155.0F, -59.6F, 12, 4, 19, 0.0F, false));
		cage.cubeList.add(new ModelBox(cage, 480, 38, -9.0F, -165.0F, -58.0F, 4, 12, 17, 0.0F, false));
		cage.cubeList.add(new ModelBox(cage, 480, 38, 5.0F, -165.0F, -58.0F, 4, 12, 17, 0.0F, false));
		cage.cubeList.add(new ModelBox(cage, 480, 38, -16.0F, -176.0F, -52.0F, 32, 32, 10, 0.0F, false));

		middle_panel_1 = new RendererModel(this);
		middle_panel_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_1.addChild(middle_panel_1);
		middle_panel_1.cubeList.add(new ModelBox(middle_panel_1, 262, 137, -24.0F, -120.2222F, -49.6667F, 48, 44, 4, 0.0F, false));
		middle_panel_1.cubeList.add(new ModelBox(middle_panel_1, 452, 38, -18.0F, -114.2222F, -51.6667F, 36, 32, 4, 0.0F, false));
		middle_panel_1.cubeList.add(new ModelBox(middle_panel_1, 452, 38, 13.0F, -113.2222F, -52.6667F, 4, 4, 4, 0.0F, false));
		middle_panel_1.cubeList.add(new ModelBox(middle_panel_1, 452, 38, -17.0F, -113.2222F, -52.6667F, 4, 4, 4, 0.0F, false));
		middle_panel_1.cubeList.add(new ModelBox(middle_panel_1, 452, 38, -17.0F, -87.2222F, -52.6667F, 4, 4, 4, 0.0F, false));
		middle_panel_1.cubeList.add(new ModelBox(middle_panel_1, 452, 38, 13.0F, -87.2222F, -52.6667F, 4, 4, 4, 0.0F, false));

		lower_panel_1 = new RendererModel(this);
		lower_panel_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_1.addChild(lower_panel_1);

		componets = new RendererModel(this);
		componets.setRotationPoint(0.0F, 0.0F, 0.0F);
		lower_panel_1.addChild(componets);
		componets.cubeList.add(new ModelBox(componets, 491, 48, -21.2F, -53.2222F, -46.0667F, 2, 2, 2, 0.0F, false));
		componets.cubeList.add(new ModelBox(componets, 491, 48, -15.0F, -53.2222F, -46.0667F, 2, 2, 2, 0.0F, false));
		componets.cubeList.add(new ModelBox(componets, 509, 276, -8.0F, -53.2222F, -48.6667F, 12, 8, 4, 0.0F, false));
		componets.cubeList.add(new ModelBox(componets, 491, 48, -4.0F, -45.2222F, -46.6667F, 2, 6, 3, 0.0F, false));
		componets.cubeList.add(new ModelBox(componets, 475, 288, -14.0F, -42.2222F, -47.2667F, 8, 8, 4, 0.0F, false));
		componets.cubeList.add(new ModelBox(componets, 491, 48, -8.0F, -39.2222F, -46.6667F, 6, 2, 3, 0.0F, false));
		componets.cubeList.add(new ModelBox(componets, 534, 310, 2.0F, -42.2222F, -47.2667F, 8, 8, 4, 0.0F, false));
		componets.cubeList.add(new ModelBox(componets, 694, 180, 11.2F, -54.2222F, -46.0667F, 8, 20, 2, 0.0F, false));
		componets.cubeList.add(new ModelBox(componets, 491, 48, -18.2F, -53.2222F, -46.0667F, 2, 2, 2, 0.0F, false));
		componets.cubeList.add(new ModelBox(componets, 491, 48, -15.0F, -50.2222F, -46.0667F, 2, 2, 2, 0.0F, false));
		componets.cubeList.add(new ModelBox(componets, 491, 48, -18.2F, -50.2222F, -46.0667F, 2, 2, 2, 0.0F, false));
		componets.cubeList.add(new ModelBox(componets, 491, 48, -21.2F, -50.2222F, -46.0667F, 2, 2, 2, 0.0F, false));
		componets.cubeList.add(new ModelBox(componets, 491, 48, -21.2F, -47.2222F, -46.0667F, 2, 2, 2, 0.0F, false));
		componets.cubeList.add(new ModelBox(componets, 491, 48, -18.2F, -47.2222F, -46.0667F, 2, 2, 2, 0.0F, false));
		componets.cubeList.add(new ModelBox(componets, 491, 48, -15.0F, -47.2222F, -46.0667F, 2, 2, 2, 0.0F, false));
		componets.cubeList.add(new ModelBox(componets, 491, 48, -2.0F, -45.2222F, -46.6667F, 2, 6, 3, 0.0F, false));
		componets.cubeList.add(new ModelBox(componets, 491, 48, -2.0F, -39.2222F, -46.6667F, 6, 2, 3, 0.0F, false));
		componets.cubeList.add(new ModelBox(componets, 704, 210, -6.0F, -37.2222F, -46.6667F, 10, 2, 3, 0.0F, false));

		panelbox = new RendererModel(this);
		panelbox.setRotationPoint(0.0F, 0.0F, 0.0F);
		lower_panel_1.addChild(panelbox);
		panelbox.cubeList.add(new ModelBox(panelbox, 665, 48, -24.0F, -57.2222F, -45.2667F, 48, 27, 0, 0.0F, false));
		panelbox.cubeList.add(new ModelBox(panelbox, 658, 52, -23.0F, -57.2222F, -49.2667F, 46, 2, 4, 0.0F, false));
		panelbox.cubeList.add(new ModelBox(panelbox, 667, 59, -23.0F, -33.2222F, -49.2667F, 46, 2, 4, 0.0F, false));

		panelbox2 = new RendererModel(this);
		panelbox2.setRotationPoint(0.0F, 0.0F, 0.0F);
		lower_panel_1.addChild(panelbox2);
		panelbox2.cubeList.add(new ModelBox(panelbox2, 669, 45, -25.0F, -57.2222F, -49.2667F, 2, 26, 4, 0.0F, false));
		panelbox2.cubeList.add(new ModelBox(panelbox2, 715, 59, 23.0F, -57.2222F, -49.2667F, 2, 26, 4, 0.0F, false));

		floorboards_1 = new RendererModel(this);
		floorboards_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_1.addChild(floorboards_1);
		floorboards_1.cubeList.add(new ModelBox(floorboards_1, 242, 185, -32.4F, -0.2222F, -58.6667F, 64, 4, 4, 0.0F, false));
		floorboards_1.cubeList.add(new ModelBox(floorboards_1, 242, 185, -26.0F, -0.2222F, -46.6667F, 52, 4, 4, 0.0F, false));
		floorboards_1.cubeList.add(new ModelBox(floorboards_1, 242, 185, -24.0F, -0.2222F, -42.6667F, 48, 4, 4, 0.0F, false));
		floorboards_1.cubeList.add(new ModelBox(floorboards_1, 242, 185, -22.0F, -0.2222F, -38.6667F, 44, 4, 4, 0.0F, false));
		floorboards_1.cubeList.add(new ModelBox(floorboards_1, 242, 185, -18.0F, -0.2222F, -34.6667F, 36, 4, 4, 0.0F, false));
		floorboards_1.cubeList.add(new ModelBox(floorboards_1, 242, 185, -16.0F, -0.2222F, -30.6667F, 32, 4, 4, 0.0F, false));
		floorboards_1.cubeList.add(new ModelBox(floorboards_1, 242, 185, -14.0F, -0.2222F, -26.6667F, 28, 4, 4, 0.0F, false));
		floorboards_1.cubeList.add(new ModelBox(floorboards_1, 242, 185, -12.0F, -0.2222F, -22.6667F, 24, 4, 4, 0.0F, false));
		floorboards_1.cubeList.add(new ModelBox(floorboards_1, 242, 185, -10.0F, -0.2222F, -18.6667F, 20, 4, 4, 0.0F, false));
		floorboards_1.cubeList.add(new ModelBox(floorboards_1, 242, 185, -8.0F, -0.2222F, -14.6667F, 16, 4, 4, 0.0F, false));
		floorboards_1.cubeList.add(new ModelBox(floorboards_1, 242, 185, -6.0F, -0.2222F, -10.6667F, 12, 4, 4, 0.0F, false));
		floorboards_1.cubeList.add(new ModelBox(floorboards_1, 242, 185, -4.0F, -0.2222F, -6.6667F, 8, 4, 4, 0.0F, false));
		floorboards_1.cubeList.add(new ModelBox(floorboards_1, 242, 185, -30.8F, -0.2222F, -54.6667F, 60, 4, 4, 0.0F, false));
		floorboards_1.cubeList.add(new ModelBox(floorboards_1, 242, 185, -28.0F, -0.2222F, -50.6667F, 56, 4, 4, 0.0F, false));

		roof_side_1 = new RendererModel(this);
		roof_side_1.setRotationPoint(0.0F, -188.0F, -48.0F);
		panel_side_1.addChild(roof_side_1);

		center_peak_1 = new RendererModel(this);
		center_peak_1.setRotationPoint(0.0F, 0.0F, 16.0F);
		setRotationAngle(center_peak_1, 0.1047F, 0.0F, 0.0F);
		roof_side_1.addChild(center_peak_1);
		center_peak_1.cubeList.add(new ModelBox(center_peak_1, 196, 306, -18.0F, -13.8222F, -1.4667F, 37, 2, 4, 0.0F, false));
		center_peak_1.cubeList.add(new ModelBox(center_peak_1, 162, 306, -6.0F, -18.2222F, 22.5333F, 12, 8, 8, 0.0F, false));
		center_peak_1.cubeList.add(new ModelBox(center_peak_1, 162, 306, -6.0F, -18.2222F, 30.5333F, 12, 4, 4, 0.0F, false));
		center_peak_1.cubeList.add(new ModelBox(center_peak_1, 162, 306, -12.0F, -12.2222F, -1.0667F, 4, 8, 8, 0.0F, false));
		center_peak_1.cubeList.add(new ModelBox(center_peak_1, 162, 306, -2.0F, -12.2222F, -1.0667F, 4, 8, 8, 0.0F, false));
		center_peak_1.cubeList.add(new ModelBox(center_peak_1, 162, 306, 8.0F, -12.2222F, -1.0667F, 4, 8, 8, 0.0F, false));
		center_peak_1.cubeList.add(new ModelBox(center_peak_1, 162, 306, -16.0F, -13.8222F, 2.5333F, 32, 2, 4, 0.0F, false));
		center_peak_1.cubeList.add(new ModelBox(center_peak_1, 114, 313, -14.0F, -13.8222F, 6.5333F, 28, 2, 4, 0.0F, false));
		center_peak_1.cubeList.add(new ModelBox(center_peak_1, 162, 306, -12.0F, -13.8222F, 10.5333F, 24, 2, 4, 0.0F, false));
		center_peak_1.cubeList.add(new ModelBox(center_peak_1, 185, 288, -10.0F, -13.8222F, 14.5333F, 20, 2, 4, 0.0F, false));
		center_peak_1.cubeList.add(new ModelBox(center_peak_1, 162, 306, -8.0F, -13.8222F, 18.5333F, 16, 2, 4, 0.0F, false));

		wroughtiron_front_1 = new RendererModel(this);
		wroughtiron_front_1.setRotationPoint(0.0F, 191.7778F, 46.3333F);
		roof_side_1.addChild(wroughtiron_front_1);
		wroughtiron_front_1.cubeList.add(new ModelBox(wroughtiron_front_1, 6, 420, -32.0F, -220.0F, -54.0F, 64, 24, 0, 0.0F, false));

		outer_peak_1 = new RendererModel(this);
		outer_peak_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(outer_peak_1, 0.0873F, 0.0F, 0.0F);
		roof_side_1.addChild(outer_peak_1);
		outer_peak_1.cubeList.add(new ModelBox(outer_peak_1, 139, 320, -32.0F, -5.0222F, -10.6667F, 64, 2, 7, 0.0F, false));
		outer_peak_1.cubeList.add(new ModelBox(outer_peak_1, 139, 320, -29.8F, -3.0222F, -7.2667F, 60, 2, 8, 0.0F, false));
		outer_peak_1.cubeList.add(new ModelBox(outer_peak_1, 139, 320, -25.6F, -5.0222F, 0.3333F, 52, 4, 4, 0.0F, false));
		outer_peak_1.cubeList.add(new ModelBox(outer_peak_1, 139, 320, -23.6F, -5.0222F, 4.3333F, 48, 4, 4, 0.0F, false));
		outer_peak_1.cubeList.add(new ModelBox(outer_peak_1, 139, 320, -21.6F, -5.0222F, 8.3333F, 44, 4, 4, 0.0F, false));
		outer_peak_1.cubeList.add(new ModelBox(outer_peak_1, 139, 320, -19.6F, -5.0222F, 12.3333F, 40, 4, 4, 0.0F, false));
		outer_peak_1.cubeList.add(new ModelBox(outer_peak_1, 139, 320, -4.0F, -24.0222F, 42.3333F, 8, 20, 4, 0.0F, false));
		outer_peak_1.cubeList.add(new ModelBox(outer_peak_1, 139, 320, -27.6F, -5.0222F, -3.6667F, 56, 4, 4, 0.0F, false));

		panel_side_2 = new RendererModel(this);
		panel_side_2.setRotationPoint(0.0F, -3.7778F, 1.6667F);
		setRotationAngle(panel_side_2, 0.0F, -2.0944F, 0.0F);
		box.addChild(panel_side_2);

		side_2 = new RendererModel(this);
		side_2.setRotationPoint(0.0F, 3.7778F, -1.6667F);
		panel_side_2.addChild(side_2);
		side_2.cubeList.add(new ModelBox(side_2, 146, 393, -28.0F, -36.0F, -49.0F, 56, 4, 4, 0.0F, false));
		side_2.cubeList.add(new ModelBox(side_2, 139, 249, -28.0F, -184.0F, -49.0F, 4, 148, 4, 0.0F, false));
		side_2.cubeList.add(new ModelBox(side_2, 144, 246, 24.0F, -184.0F, -49.0F, 4, 148, 4, 0.0F, false));
		side_2.cubeList.add(new ModelBox(side_2, 235, 246, -28.0F, -192.0F, -49.0F, 56, 8, 4, 0.0F, false));
		side_2.cubeList.add(new ModelBox(side_2, 171, 352, -24.0F, -136.0F, -49.0F, 48, 12, 4, 0.0F, false));
		side_2.cubeList.add(new ModelBox(side_2, 278, 374, -24.0F, -80.0F, -49.0F, 48, 20, 4, 0.0F, false));
		side_2.cubeList.add(new ModelBox(side_2, 118, 267, -28.0F, -195.2F, -51.0F, 56, 4, 4, 0.0F, false));
		side_2.cubeList.add(new ModelBox(side_2, 452, 18, -28.0F, -132.0F, -50.6F, 56, 4, 6, 0.0F, false));
		side_2.cubeList.add(new ModelBox(side_2, 448, 13, -28.0F, -76.0F, -50.6F, 56, 4, 6, 0.0F, false));
		side_2.cubeList.add(new ModelBox(side_2, 448, 84, -28.0F, -66.0F, -50.6F, 56, 4, 6, 0.0F, false));
		side_2.cubeList.add(new ModelBox(side_2, 459, 6, -28.0F, -33.0F, -50.6F, 56, 4, 6, 0.0F, false));
		side_2.cubeList.add(new ModelBox(side_2, 441, 93, -28.0F, -16.0F, -50.6F, 56, 4, 6, 0.0F, false));
		side_2.cubeList.add(new ModelBox(side_2, 288, 422, -28.0F, -32.0F, -49.0F, 56, 16, 4, 0.0F, false));
		side_2.cubeList.add(new ModelBox(side_2, 140, 392, -28.0F, -12.0F, -49.0F, 56, 8, 4, 0.0F, false));

		top_panel_2 = new RendererModel(this);
		top_panel_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_2.addChild(top_panel_2);
		top_panel_2.cubeList.add(new ModelBox(top_panel_2, 260, 82, -24.0F, -180.2222F, -49.6667F, 48, 48, 4, 0.0F, false));

		cage2 = new RendererModel(this);
		cage2.setRotationPoint(0.0F, 3.7778F, -1.6667F);
		top_panel_2.addChild(cage2);
		cage2.cubeList.add(new ModelBox(cage2, 480, 38, -9.0F, -177.0F, -58.0F, 4, 12, 17, 0.0F, false));
		cage2.cubeList.add(new ModelBox(cage2, 480, 38, 5.0F, -177.0F, -58.0F, 4, 8, 17, 0.0F, false));
		cage2.cubeList.add(new ModelBox(cage2, 480, 38, -17.0F, -169.0F, -58.6F, 11, 4, 18, 0.0F, false));
		cage2.cubeList.add(new ModelBox(cage2, 480, 38, -17.0F, -155.0F, -58.6F, 11, 4, 18, 0.0F, false));
		cage2.cubeList.add(new ModelBox(cage2, 480, 38, 5.0F, -151.0F, -58.0F, 4, 8, 17, 0.0F, false));
		cage2.cubeList.add(new ModelBox(cage2, 480, 38, -9.0F, -151.0F, -58.0F, 4, 8, 17, 0.0F, false));
		cage2.cubeList.add(new ModelBox(cage2, 480, 38, 6.0F, -155.0F, -58.6F, 11, 4, 18, 0.0F, false));
		cage2.cubeList.add(new ModelBox(cage2, 480, 38, 6.0F, -169.0F, -58.6F, 11, 4, 18, 0.0F, false));
		cage2.cubeList.add(new ModelBox(cage2, 480, 38, -6.0F, -169.0F, -59.6F, 12, 4, 19, 0.0F, false));
		cage2.cubeList.add(new ModelBox(cage2, 480, 38, -6.0F, -155.0F, -59.6F, 12, 4, 19, 0.0F, false));
		cage2.cubeList.add(new ModelBox(cage2, 480, 38, -9.0F, -165.0F, -58.0F, 4, 12, 17, 0.0F, false));
		cage2.cubeList.add(new ModelBox(cage2, 480, 38, 5.0F, -165.0F, -58.0F, 4, 12, 17, 0.0F, false));
		cage2.cubeList.add(new ModelBox(cage2, 480, 38, -16.0F, -176.0F, -52.0F, 32, 32, 10, 0.0F, false));

		middle_panel_2 = new RendererModel(this);
		middle_panel_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_2.addChild(middle_panel_2);
		middle_panel_2.cubeList.add(new ModelBox(middle_panel_2, 276, 137, -24.0F, -120.2222F, -49.6667F, 48, 44, 4, 0.0F, false));
		middle_panel_2.cubeList.add(new ModelBox(middle_panel_2, 452, 38, -18.0F, -114.2222F, -51.6667F, 36, 32, 4, 0.0F, false));
		middle_panel_2.cubeList.add(new ModelBox(middle_panel_2, 452, 38, 13.0F, -113.2222F, -52.6667F, 4, 4, 4, 0.0F, false));
		middle_panel_2.cubeList.add(new ModelBox(middle_panel_2, 452, 38, -17.0F, -113.2222F, -52.6667F, 4, 4, 4, 0.0F, false));
		middle_panel_2.cubeList.add(new ModelBox(middle_panel_2, 452, 38, -17.0F, -87.2222F, -52.6667F, 4, 4, 4, 0.0F, false));
		middle_panel_2.cubeList.add(new ModelBox(middle_panel_2, 452, 38, 13.0F, -87.2222F, -52.6667F, 4, 4, 4, 0.0F, false));

		lower_panel_2 = new RendererModel(this);
		lower_panel_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_2.addChild(lower_panel_2);

		componets2 = new RendererModel(this);
		componets2.setRotationPoint(0.0F, 0.0F, 0.0F);
		lower_panel_2.addChild(componets2);
		componets2.cubeList.add(new ModelBox(componets2, 571, 157, -21.2F, -53.2222F, -46.0667F, 2, 2, 2, 0.0F, false));
		componets2.cubeList.add(new ModelBox(componets2, 582, 137, -15.0F, -53.2222F, -46.0667F, 2, 2, 2, 0.0F, false));
		componets2.cubeList.add(new ModelBox(componets2, 576, 153, -21.0F, -41.2222F, -46.0667F, 2, 2, 2, 0.0F, false));
		componets2.cubeList.add(new ModelBox(componets2, 573, 155, -15.0F, -50.2222F, -46.0667F, 2, 2, 2, 0.0F, false));
		componets2.cubeList.add(new ModelBox(componets2, 564, 148, -21.0F, -38.2222F, -46.0667F, 2, 2, 2, 0.0F, false));
		componets2.cubeList.add(new ModelBox(componets2, 564, 146, -21.2F, -50.2222F, -46.0667F, 2, 2, 2, 0.0F, false));
		componets2.cubeList.add(new ModelBox(componets2, 576, 157, -21.2F, -47.2222F, -46.0667F, 2, 2, 2, 0.0F, false));
		componets2.cubeList.add(new ModelBox(componets2, 585, 150, -21.0F, -35.2222F, -46.0667F, 2, 2, 2, 0.0F, false));
		componets2.cubeList.add(new ModelBox(componets2, 571, 141, -15.0F, -47.2222F, -46.0667F, 2, 2, 2, 0.0F, false));
		componets2.cubeList.add(new ModelBox(componets2, 505, 52, -3.8F, -50.0222F, -46.6667F, 20, 2, 3, 0.0F, false));
		componets2.cubeList.add(new ModelBox(componets2, 505, 52, 3.0F, -44.2222F, -46.6667F, 20, 2, 3, 0.0F, false));
		componets2.cubeList.add(new ModelBox(componets2, 678, 180, 16.2F, -51.0222F, -46.6667F, 4, 4, 3, 0.0F, false));
		componets2.cubeList.add(new ModelBox(componets2, 505, 52, -5.8F, -50.0222F, -46.6667F, 2, 8, 3, 0.0F, false));
		componets2.cubeList.add(new ModelBox(componets2, 683, 155, -0.8F, -45.0222F, -46.6667F, 4, 4, 3, 0.0F, false));
		componets2.cubeList.add(new ModelBox(componets2, 505, 52, -25.8F, -44.0222F, -46.6667F, 20, 2, 3, 0.0F, false));
		componets2.cubeList.add(new ModelBox(componets2, 505, 52, -13.8F, -42.0222F, -46.6667F, 2, 6, 3, 0.0F, false));
		componets2.cubeList.add(new ModelBox(componets2, 505, 52, -13.8F, -38.0222F, -46.6667F, 26, 2, 3, 0.0F, false));
		componets2.cubeList.add(new ModelBox(componets2, 505, 52, 10.2F, -36.0222F, -46.6667F, 2, 4, 3, 0.0F, false));

		panelbox3 = new RendererModel(this);
		panelbox3.setRotationPoint(0.0F, 0.0F, 0.0F);
		lower_panel_2.addChild(panelbox3);
		panelbox3.cubeList.add(new ModelBox(panelbox3, 674, 41, -24.0F, -57.2222F, -45.2667F, 48, 27, 0, 0.0F, false));
		panelbox3.cubeList.add(new ModelBox(panelbox3, 692, 43, -23.0F, -57.2222F, -49.2667F, 46, 2, 4, 0.0F, false));
		panelbox3.cubeList.add(new ModelBox(panelbox3, 660, 57, -23.0F, -33.2222F, -49.2667F, 46, 2, 4, 0.0F, false));

		panelbox4 = new RendererModel(this);
		panelbox4.setRotationPoint(0.0F, 0.0F, 0.0F);
		lower_panel_2.addChild(panelbox4);
		panelbox4.cubeList.add(new ModelBox(panelbox4, 683, 57, -25.0F, -57.2222F, -49.2667F, 2, 26, 4, 0.0F, false));
		panelbox4.cubeList.add(new ModelBox(panelbox4, 706, 43, 23.0F, -57.2222F, -49.2667F, 2, 26, 4, 0.0F, false));

		floorboards_2 = new RendererModel(this);
		floorboards_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_2.addChild(floorboards_2);
		floorboards_2.cubeList.add(new ModelBox(floorboards_2, 240, 178, -32.4F, -0.2222F, -58.6667F, 64, 4, 4, 0.0F, false));
		floorboards_2.cubeList.add(new ModelBox(floorboards_2, 240, 178, -26.0F, -0.2222F, -46.6667F, 52, 4, 4, 0.0F, false));
		floorboards_2.cubeList.add(new ModelBox(floorboards_2, 240, 178, -24.0F, -0.2222F, -42.6667F, 48, 4, 4, 0.0F, false));
		floorboards_2.cubeList.add(new ModelBox(floorboards_2, 240, 178, -22.0F, -0.2222F, -38.6667F, 44, 4, 4, 0.0F, false));
		floorboards_2.cubeList.add(new ModelBox(floorboards_2, 240, 178, -18.0F, -0.2222F, -34.6667F, 36, 4, 4, 0.0F, false));
		floorboards_2.cubeList.add(new ModelBox(floorboards_2, 240, 178, -16.0F, -0.2222F, -30.6667F, 32, 4, 4, 0.0F, false));
		floorboards_2.cubeList.add(new ModelBox(floorboards_2, 240, 178, -14.0F, -0.2222F, -26.6667F, 28, 4, 4, 0.0F, false));
		floorboards_2.cubeList.add(new ModelBox(floorboards_2, 240, 178, -12.0F, -0.2222F, -22.6667F, 24, 4, 4, 0.0F, false));
		floorboards_2.cubeList.add(new ModelBox(floorboards_2, 240, 178, -10.0F, -0.2222F, -18.6667F, 20, 4, 4, 0.0F, false));
		floorboards_2.cubeList.add(new ModelBox(floorboards_2, 240, 178, -8.0F, -0.2222F, -14.6667F, 16, 4, 4, 0.0F, false));
		floorboards_2.cubeList.add(new ModelBox(floorboards_2, 240, 178, -6.0F, -0.2222F, -10.6667F, 12, 4, 4, 0.0F, false));
		floorboards_2.cubeList.add(new ModelBox(floorboards_2, 240, 178, -4.0F, -0.2222F, -6.6667F, 8, 4, 4, 0.0F, false));
		floorboards_2.cubeList.add(new ModelBox(floorboards_2, 240, 178, -30.8F, -0.2222F, -54.6667F, 60, 4, 4, 0.0F, false));
		floorboards_2.cubeList.add(new ModelBox(floorboards_2, 240, 178, -28.0F, -0.2222F, -50.6667F, 56, 4, 4, 0.0F, false));

		roof_side_2 = new RendererModel(this);
		roof_side_2.setRotationPoint(0.0F, -188.0F, -48.0F);
		panel_side_2.addChild(roof_side_2);

		center_peak_2 = new RendererModel(this);
		center_peak_2.setRotationPoint(0.0F, 0.0F, 16.0F);
		setRotationAngle(center_peak_2, 0.1047F, 0.0F, 0.0F);
		roof_side_2.addChild(center_peak_2);
		center_peak_2.cubeList.add(new ModelBox(center_peak_2, 196, 306, -18.0F, -13.8222F, -1.4667F, 37, 2, 4, 0.0F, false));
		center_peak_2.cubeList.add(new ModelBox(center_peak_2, 162, 306, -6.0F, -18.2222F, 22.5333F, 12, 8, 8, 0.0F, false));
		center_peak_2.cubeList.add(new ModelBox(center_peak_2, 162, 306, -6.0F, -18.2222F, 30.5333F, 12, 4, 4, 0.0F, false));
		center_peak_2.cubeList.add(new ModelBox(center_peak_2, 162, 306, -12.0F, -12.2222F, -1.0667F, 4, 8, 8, 0.0F, false));
		center_peak_2.cubeList.add(new ModelBox(center_peak_2, 162, 306, -2.0F, -12.2222F, -1.0667F, 4, 8, 8, 0.0F, false));
		center_peak_2.cubeList.add(new ModelBox(center_peak_2, 162, 306, 8.0F, -12.2222F, -1.0667F, 4, 8, 8, 0.0F, false));
		center_peak_2.cubeList.add(new ModelBox(center_peak_2, 162, 306, -16.0F, -13.8222F, 2.5333F, 32, 2, 4, 0.0F, false));
		center_peak_2.cubeList.add(new ModelBox(center_peak_2, 114, 313, -14.0F, -13.8222F, 6.5333F, 28, 2, 4, 0.0F, false));
		center_peak_2.cubeList.add(new ModelBox(center_peak_2, 162, 306, -12.0F, -13.8222F, 10.5333F, 24, 2, 4, 0.0F, false));
		center_peak_2.cubeList.add(new ModelBox(center_peak_2, 185, 288, -10.0F, -13.8222F, 14.5333F, 20, 2, 4, 0.0F, false));
		center_peak_2.cubeList.add(new ModelBox(center_peak_2, 162, 306, -8.0F, -13.8222F, 18.5333F, 16, 2, 4, 0.0F, false));

		wroughtiron_front_2 = new RendererModel(this);
		wroughtiron_front_2.setRotationPoint(0.0F, 191.7778F, 46.3333F);
		roof_side_2.addChild(wroughtiron_front_2);
		wroughtiron_front_2.cubeList.add(new ModelBox(wroughtiron_front_2, 6, 420, -32.0F, -220.0F, -54.0F, 64, 24, 0, 0.0F, false));

		outer_peak_2 = new RendererModel(this);
		outer_peak_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(outer_peak_2, 0.0873F, 0.0F, 0.0F);
		roof_side_2.addChild(outer_peak_2);
		outer_peak_2.cubeList.add(new ModelBox(outer_peak_2, 139, 320, -32.0F, -5.0222F, -10.6667F, 64, 2, 7, 0.0F, false));
		outer_peak_2.cubeList.add(new ModelBox(outer_peak_2, 139, 320, -29.8F, -3.0222F, -7.2667F, 60, 2, 8, 0.0F, false));
		outer_peak_2.cubeList.add(new ModelBox(outer_peak_2, 139, 320, -25.6F, -5.0222F, 0.3333F, 52, 4, 4, 0.0F, false));
		outer_peak_2.cubeList.add(new ModelBox(outer_peak_2, 139, 320, -23.6F, -5.0222F, 4.3333F, 48, 4, 4, 0.0F, false));
		outer_peak_2.cubeList.add(new ModelBox(outer_peak_2, 139, 320, -21.6F, -5.0222F, 8.3333F, 44, 4, 4, 0.0F, false));
		outer_peak_2.cubeList.add(new ModelBox(outer_peak_2, 139, 320, -19.6F, -5.0222F, 12.3333F, 40, 4, 4, 0.0F, false));
		outer_peak_2.cubeList.add(new ModelBox(outer_peak_2, 139, 320, -4.0F, -24.0222F, 42.3333F, 8, 20, 4, 0.0F, false));
		outer_peak_2.cubeList.add(new ModelBox(outer_peak_2, 139, 320, -27.6F, -5.0222F, -3.6667F, 56, 4, 4, 0.0F, false));

		panel_side_3 = new RendererModel(this);
		panel_side_3.setRotationPoint(0.0F, -3.7778F, 1.6667F);
		setRotationAngle(panel_side_3, 0.0F, 3.1416F, 0.0F);
		box.addChild(panel_side_3);

		roof_side_3 = new RendererModel(this);
		roof_side_3.setRotationPoint(0.0F, -188.0F, -48.0F);
		panel_side_3.addChild(roof_side_3);

		center_peak_3 = new RendererModel(this);
		center_peak_3.setRotationPoint(0.0F, 0.0F, 16.0F);
		setRotationAngle(center_peak_3, 0.1047F, 0.0F, 0.0F);
		roof_side_3.addChild(center_peak_3);
		center_peak_3.cubeList.add(new ModelBox(center_peak_3, 196, 306, -18.0F, -13.8222F, -1.4667F, 37, 2, 4, 0.0F, false));
		center_peak_3.cubeList.add(new ModelBox(center_peak_3, 162, 306, -6.0F, -18.2222F, 22.5333F, 12, 8, 8, 0.0F, false));
		center_peak_3.cubeList.add(new ModelBox(center_peak_3, 162, 306, -6.0F, -18.2222F, 30.5333F, 12, 4, 4, 0.0F, false));
		center_peak_3.cubeList.add(new ModelBox(center_peak_3, 162, 306, -12.0F, -12.2222F, -1.0667F, 4, 8, 8, 0.0F, false));
		center_peak_3.cubeList.add(new ModelBox(center_peak_3, 162, 306, -2.0F, -12.2222F, -1.0667F, 4, 8, 8, 0.0F, false));
		center_peak_3.cubeList.add(new ModelBox(center_peak_3, 162, 306, 8.0F, -12.2222F, -1.0667F, 4, 8, 8, 0.0F, false));
		center_peak_3.cubeList.add(new ModelBox(center_peak_3, 162, 306, -16.0F, -13.8222F, 2.5333F, 32, 2, 4, 0.0F, false));
		center_peak_3.cubeList.add(new ModelBox(center_peak_3, 114, 313, -14.0F, -13.8222F, 6.5333F, 28, 2, 4, 0.0F, false));
		center_peak_3.cubeList.add(new ModelBox(center_peak_3, 162, 306, -12.0F, -13.8222F, 10.5333F, 24, 2, 4, 0.0F, false));
		center_peak_3.cubeList.add(new ModelBox(center_peak_3, 185, 288, -10.0F, -13.8222F, 14.5333F, 20, 2, 4, 0.0F, false));
		center_peak_3.cubeList.add(new ModelBox(center_peak_3, 162, 306, -8.0F, -13.8222F, 18.5333F, 16, 2, 4, 0.0F, false));

		wroughtiron_front_3 = new RendererModel(this);
		wroughtiron_front_3.setRotationPoint(0.0F, 191.7778F, 46.3333F);
		roof_side_3.addChild(wroughtiron_front_3);
		wroughtiron_front_3.cubeList.add(new ModelBox(wroughtiron_front_3, 6, 420, -32.0F, -220.0F, -54.0F, 64, 24, 0, 0.0F, false));

		outer_peak_3 = new RendererModel(this);
		outer_peak_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(outer_peak_3, 0.0873F, 0.0F, 0.0F);
		roof_side_3.addChild(outer_peak_3);
		outer_peak_3.cubeList.add(new ModelBox(outer_peak_3, 139, 320, -32.0F, -5.0222F, -10.6667F, 64, 2, 7, 0.0F, false));
		outer_peak_3.cubeList.add(new ModelBox(outer_peak_3, 139, 320, -29.8F, -3.0222F, -7.2667F, 60, 2, 8, 0.0F, false));
		outer_peak_3.cubeList.add(new ModelBox(outer_peak_3, 139, 320, -25.6F, -5.0222F, 0.3333F, 52, 4, 4, 0.0F, false));
		outer_peak_3.cubeList.add(new ModelBox(outer_peak_3, 139, 320, -23.6F, -5.0222F, 4.3333F, 48, 4, 4, 0.0F, false));
		outer_peak_3.cubeList.add(new ModelBox(outer_peak_3, 139, 320, -21.6F, -5.0222F, 8.3333F, 44, 4, 4, 0.0F, false));
		outer_peak_3.cubeList.add(new ModelBox(outer_peak_3, 139, 320, -19.6F, -5.0222F, 12.3333F, 40, 4, 4, 0.0F, false));
		outer_peak_3.cubeList.add(new ModelBox(outer_peak_3, 139, 320, -4.0F, -24.0222F, 42.3333F, 8, 20, 4, 0.0F, false));
		outer_peak_3.cubeList.add(new ModelBox(outer_peak_3, 139, 320, -27.6F, -5.0222F, -3.6667F, 56, 4, 4, 0.0F, false));

		side_3 = new RendererModel(this);
		side_3.setRotationPoint(0.0F, 3.7778F, -1.6667F);
		panel_side_3.addChild(side_3);
		side_3.cubeList.add(new ModelBox(side_3, 146, 393, -28.0F, -36.0F, -49.0F, 56, 4, 4, 0.0F, false));
		side_3.cubeList.add(new ModelBox(side_3, 139, 249, -28.0F, -184.0F, -49.0F, 4, 148, 4, 0.0F, false));
		side_3.cubeList.add(new ModelBox(side_3, 144, 246, 24.0F, -184.0F, -49.0F, 4, 148, 4, 0.0F, false));
		side_3.cubeList.add(new ModelBox(side_3, 235, 246, -28.0F, -192.0F, -49.0F, 56, 8, 4, 0.0F, false));
		side_3.cubeList.add(new ModelBox(side_3, 171, 352, -24.0F, -136.0F, -49.0F, 48, 12, 4, 0.0F, false));
		side_3.cubeList.add(new ModelBox(side_3, 278, 374, -24.0F, -80.0F, -49.0F, 48, 20, 4, 0.0F, false));
		side_3.cubeList.add(new ModelBox(side_3, 118, 267, -28.0F, -195.2F, -51.0F, 56, 4, 4, 0.0F, false));
		side_3.cubeList.add(new ModelBox(side_3, 452, 18, -28.0F, -132.0F, -50.6F, 56, 4, 6, 0.0F, false));
		side_3.cubeList.add(new ModelBox(side_3, 448, 13, -28.0F, -76.0F, -50.6F, 56, 4, 6, 0.0F, false));
		side_3.cubeList.add(new ModelBox(side_3, 448, 84, -28.0F, -66.0F, -50.6F, 56, 4, 6, 0.0F, false));
		side_3.cubeList.add(new ModelBox(side_3, 459, 6, -28.0F, -33.0F, -50.6F, 56, 4, 6, 0.0F, false));
		side_3.cubeList.add(new ModelBox(side_3, 441, 93, -28.0F, -16.0F, -50.6F, 56, 4, 6, 0.0F, false));
		side_3.cubeList.add(new ModelBox(side_3, 288, 422, -28.0F, -32.0F, -49.0F, 56, 16, 4, 0.0F, false));
		side_3.cubeList.add(new ModelBox(side_3, 140, 392, -28.0F, -12.0F, -49.0F, 56, 8, 4, 0.0F, false));

		top_panel_3 = new RendererModel(this);
		top_panel_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_3.addChild(top_panel_3);
		top_panel_3.cubeList.add(new ModelBox(top_panel_3, 272, 70, -24.0F, -180.2222F, -49.6667F, 48, 48, 4, 0.0F, false));

		cage3 = new RendererModel(this);
		cage3.setRotationPoint(0.0F, 3.7778F, -1.6667F);
		top_panel_3.addChild(cage3);
		cage3.cubeList.add(new ModelBox(cage3, 480, 38, -9.0F, -177.0F, -58.0F, 4, 12, 17, 0.0F, false));
		cage3.cubeList.add(new ModelBox(cage3, 480, 38, 5.0F, -177.0F, -58.0F, 4, 8, 17, 0.0F, false));
		cage3.cubeList.add(new ModelBox(cage3, 480, 38, -17.0F, -169.0F, -58.6F, 11, 4, 18, 0.0F, false));
		cage3.cubeList.add(new ModelBox(cage3, 480, 38, -17.0F, -155.0F, -58.6F, 11, 4, 18, 0.0F, false));
		cage3.cubeList.add(new ModelBox(cage3, 480, 38, 5.0F, -151.0F, -58.0F, 4, 8, 17, 0.0F, false));
		cage3.cubeList.add(new ModelBox(cage3, 480, 38, -9.0F, -151.0F, -58.0F, 4, 8, 17, 0.0F, false));
		cage3.cubeList.add(new ModelBox(cage3, 480, 38, 6.0F, -155.0F, -58.6F, 11, 4, 18, 0.0F, false));
		cage3.cubeList.add(new ModelBox(cage3, 480, 38, 6.0F, -169.0F, -58.6F, 11, 4, 18, 0.0F, false));
		cage3.cubeList.add(new ModelBox(cage3, 480, 38, -6.0F, -169.0F, -59.6F, 12, 4, 19, 0.0F, false));
		cage3.cubeList.add(new ModelBox(cage3, 480, 38, -6.0F, -155.0F, -59.6F, 12, 4, 19, 0.0F, false));
		cage3.cubeList.add(new ModelBox(cage3, 480, 38, -9.0F, -165.0F, -58.0F, 4, 12, 17, 0.0F, false));
		cage3.cubeList.add(new ModelBox(cage3, 480, 38, 5.0F, -165.0F, -58.0F, 4, 12, 17, 0.0F, false));
		cage3.cubeList.add(new ModelBox(cage3, 480, 38, -16.0F, -176.0F, -52.0F, 32, 32, 10, 0.0F, false));

		middle_panel_3 = new RendererModel(this);
		middle_panel_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_3.addChild(middle_panel_3);
		middle_panel_3.cubeList.add(new ModelBox(middle_panel_3, 265, 134, -24.0F, -120.2222F, -49.6667F, 48, 44, 4, 0.0F, false));
		middle_panel_3.cubeList.add(new ModelBox(middle_panel_3, 452, 38, -18.0F, -114.2222F, -51.6667F, 36, 32, 4, 0.0F, false));
		middle_panel_3.cubeList.add(new ModelBox(middle_panel_3, 452, 38, 13.0F, -113.2222F, -52.6667F, 4, 4, 4, 0.0F, false));
		middle_panel_3.cubeList.add(new ModelBox(middle_panel_3, 452, 38, -17.0F, -113.2222F, -52.6667F, 4, 4, 4, 0.0F, false));
		middle_panel_3.cubeList.add(new ModelBox(middle_panel_3, 452, 38, -17.0F, -87.2222F, -52.6667F, 4, 4, 4, 0.0F, false));
		middle_panel_3.cubeList.add(new ModelBox(middle_panel_3, 452, 38, 13.0F, -87.2222F, -52.6667F, 4, 4, 4, 0.0F, false));

		lower_panel_3 = new RendererModel(this);
		lower_panel_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_3.addChild(lower_panel_3);

		componets3 = new RendererModel(this);
		componets3.setRotationPoint(0.0F, 0.0F, 0.0F);
		lower_panel_3.addChild(componets3);
		componets3.cubeList.add(new ModelBox(componets3, 496, 290, -21.2F, -54.4222F, -46.6667F, 8, 8, 4, 0.0F, false));
		componets3.cubeList.add(new ModelBox(componets3, 454, 201, -20.8F, -53.8222F, -47.6667F, 7, 7, 4, 0.0F, false));
		componets3.cubeList.add(new ModelBox(componets3, 491, 38, -3.2F, -58.4222F, -46.6667F, 1, 28, 4, 0.0F, false));
		componets3.cubeList.add(new ModelBox(componets3, 491, 38, -5.2F, -58.4222F, -46.6667F, 1, 28, 4, 0.0F, false));
		componets3.cubeList.add(new ModelBox(componets3, 491, 38, -9.4F, -58.4222F, -46.6667F, 1, 8, 4, 0.0F, false));
		componets3.cubeList.add(new ModelBox(componets3, 491, 38, -13.4F, -50.4222F, -46.6667F, 5, 1, 4, 0.0F, false));
		componets3.cubeList.add(new ModelBox(componets3, 491, 38, -17.4F, -46.4222F, -46.6667F, 1, 16, 4, 0.0F, false));
		componets3.cubeList.add(new ModelBox(componets3, 692, 166, -14.2F, -42.4222F, -46.6667F, 4, 4, 4, 0.0F, false));
		componets3.cubeList.add(new ModelBox(componets3, 694, 155, 0.6F, -38.4222F, -46.6667F, 4, 4, 4, 0.0F, false));
		componets3.cubeList.add(new ModelBox(componets3, 665, 171, 0.6F, -54.4222F, -46.6667F, 4, 4, 4, 0.0F, false));
		componets3.cubeList.add(new ModelBox(componets3, 491, 38, -10.2F, -40.6222F, -46.6667F, 5, 1, 4, 0.0F, false));
		componets3.cubeList.add(new ModelBox(componets3, 491, 38, 2.0F, -50.4222F, -46.6667F, 1, 12, 4, 0.0F, false));

		gear = new RendererModel(this);
		gear.setRotationPoint(6.8F, -42.4222F, -45.8334F);
		setRotationAngle(gear, 0.0F, 0.0F, -0.6109F);
		componets3.addChild(gear);
		gear.cubeList.add(new ModelBox(gear, 491, 38, 9.2F, -1.2F, -1.8333F, 2, 4, 4, 0.0F, false));
		gear.cubeList.add(new ModelBox(gear, 491, 38, 1.2F, -3.2F, -1.8333F, 8, 8, 4, 0.0F, false));
		gear.cubeList.add(new ModelBox(gear, 491, 38, 3.2F, -5.2F, -1.8333F, 4, 2, 4, 0.0F, false));
		gear.cubeList.add(new ModelBox(gear, 491, 38, -0.8F, -1.2F, -1.8333F, 2, 4, 4, 0.0F, false));
		gear.cubeList.add(new ModelBox(gear, 491, 38, 3.2F, 4.8F, -1.8333F, 4, 2, 4, 0.0F, false));
		gear.cubeList.add(new ModelBox(gear, 704, 48, 4.2F, -0.2F, -2.8333F, 2, 2, 4, 0.0F, false));

		gear2 = new RendererModel(this);
		gear2.setRotationPoint(6.8F, -42.4222F, -45.8334F);
		setRotationAngle(gear2, 0.0F, 0.0F, -0.6109F);
		componets3.addChild(gear2);
		gear2.cubeList.add(new ModelBox(gear2, 491, 38, 20.0F, -6.0F, -1.8333F, 2, 4, 4, 0.0F, false));
		gear2.cubeList.add(new ModelBox(gear2, 491, 38, 12.0F, -8.0F, -1.8333F, 8, 8, 4, 0.0F, false));
		gear2.cubeList.add(new ModelBox(gear2, 491, 38, 14.0F, -10.0F, -1.8333F, 4, 2, 4, 0.0F, false));
		gear2.cubeList.add(new ModelBox(gear2, 491, 38, 10.0F, -6.0F, -1.8333F, 2, 4, 4, 0.0F, false));
		gear2.cubeList.add(new ModelBox(gear2, 491, 38, 14.0F, 0.0F, -1.8333F, 4, 2, 4, 0.0F, false));
		gear2.cubeList.add(new ModelBox(gear2, 699, 45, 15.0F, -5.0F, -2.8333F, 2, 2, 4, 0.0F, false));

		gear3 = new RendererModel(this);
		gear3.setRotationPoint(6.8F, -42.4222F, -45.8334F);
		setRotationAngle(gear3, 0.0F, 0.0F, -0.6109F);
		componets3.addChild(gear3);
		gear3.cubeList.add(new ModelBox(gear3, 491, 38, 13.6F, 9.2F, -1.8333F, 2, 4, 4, 0.0F, false));
		gear3.cubeList.add(new ModelBox(gear3, 491, 38, 5.6F, 7.2F, -1.8333F, 8, 8, 4, 0.0F, false));
		gear3.cubeList.add(new ModelBox(gear3, 491, 38, 7.6F, 5.2F, -1.8333F, 4, 2, 4, 0.0F, false));
		gear3.cubeList.add(new ModelBox(gear3, 491, 38, 3.6F, 9.2F, -1.8333F, 2, 4, 4, 0.0F, false));
		gear3.cubeList.add(new ModelBox(gear3, 491, 38, 7.6F, 15.2F, -1.8333F, 4, 2, 4, 0.0F, false));
		gear3.cubeList.add(new ModelBox(gear3, 724, 57, 8.6F, 10.2F, -2.8333F, 2, 2, 4, 0.0F, false));

		panelbox5 = new RendererModel(this);
		panelbox5.setRotationPoint(0.0F, 0.0F, 0.0F);
		lower_panel_3.addChild(panelbox5);
		panelbox5.cubeList.add(new ModelBox(panelbox5, 649, 36, -24.0F, -57.2222F, -45.2667F, 48, 27, 0, 0.0F, false));
		panelbox5.cubeList.add(new ModelBox(panelbox5, 651, 50, -23.0F, -57.2222F, -49.2667F, 46, 2, 4, 0.0F, false));
		panelbox5.cubeList.add(new ModelBox(panelbox5, 674, 57, -23.0F, -33.2222F, -49.2667F, 46, 2, 4, 0.0F, false));

		panelbox6 = new RendererModel(this);
		panelbox6.setRotationPoint(0.0F, 0.0F, 0.0F);
		lower_panel_3.addChild(panelbox6);
		panelbox6.cubeList.add(new ModelBox(panelbox6, 683, 41, -25.0F, -57.2222F, -49.2667F, 2, 26, 4, 0.0F, false));
		panelbox6.cubeList.add(new ModelBox(panelbox6, 676, 27, 23.0F, -57.2222F, -49.2667F, 2, 26, 4, 0.0F, false));

		floorboards_3 = new RendererModel(this);
		floorboards_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_3.addChild(floorboards_3);
		floorboards_3.cubeList.add(new ModelBox(floorboards_3, 246, 189, -32.4F, -0.2222F, -58.6667F, 64, 4, 4, 0.0F, false));
		floorboards_3.cubeList.add(new ModelBox(floorboards_3, 246, 189, -26.0F, -0.2222F, -46.6667F, 52, 4, 4, 0.0F, false));
		floorboards_3.cubeList.add(new ModelBox(floorboards_3, 246, 189, -24.0F, -0.2222F, -42.6667F, 48, 4, 4, 0.0F, false));
		floorboards_3.cubeList.add(new ModelBox(floorboards_3, 246, 189, -22.0F, -0.2222F, -38.6667F, 44, 4, 4, 0.0F, false));
		floorboards_3.cubeList.add(new ModelBox(floorboards_3, 246, 189, -18.0F, -0.2222F, -34.6667F, 36, 4, 4, 0.0F, false));
		floorboards_3.cubeList.add(new ModelBox(floorboards_3, 246, 189, -16.0F, -0.2222F, -30.6667F, 32, 4, 4, 0.0F, false));
		floorboards_3.cubeList.add(new ModelBox(floorboards_3, 246, 189, -14.0F, -0.2222F, -26.6667F, 28, 4, 4, 0.0F, false));
		floorboards_3.cubeList.add(new ModelBox(floorboards_3, 246, 189, -12.0F, -0.2222F, -22.6667F, 24, 4, 4, 0.0F, false));
		floorboards_3.cubeList.add(new ModelBox(floorboards_3, 246, 189, -10.0F, -0.2222F, -18.6667F, 20, 4, 4, 0.0F, false));
		floorboards_3.cubeList.add(new ModelBox(floorboards_3, 246, 189, -8.0F, -0.2222F, -14.6667F, 16, 4, 4, 0.0F, false));
		floorboards_3.cubeList.add(new ModelBox(floorboards_3, 246, 189, -6.0F, -0.2222F, -10.6667F, 12, 4, 4, 0.0F, false));
		floorboards_3.cubeList.add(new ModelBox(floorboards_3, 246, 189, -4.0F, -0.2222F, -6.6667F, 8, 4, 4, 0.0F, false));
		floorboards_3.cubeList.add(new ModelBox(floorboards_3, 246, 189, -30.8F, -0.2222F, -54.6667F, 60, 4, 4, 0.0F, false));
		floorboards_3.cubeList.add(new ModelBox(floorboards_3, 246, 189, -28.0F, -0.2222F, -50.6667F, 56, 4, 4, 0.0F, false));

		panel_side_4 = new RendererModel(this);
		panel_side_4.setRotationPoint(0.0F, -3.7778F, 1.6667F);
		setRotationAngle(panel_side_4, 0.0F, 2.0944F, 0.0F);
		box.addChild(panel_side_4);

		roof_side_4 = new RendererModel(this);
		roof_side_4.setRotationPoint(0.0F, -188.0F, -48.0F);
		panel_side_4.addChild(roof_side_4);

		center_peak_4 = new RendererModel(this);
		center_peak_4.setRotationPoint(0.0F, 0.0F, 16.0F);
		setRotationAngle(center_peak_4, 0.1047F, 0.0F, 0.0F);
		roof_side_4.addChild(center_peak_4);
		center_peak_4.cubeList.add(new ModelBox(center_peak_4, 196, 306, -18.0F, -13.8222F, -1.4667F, 37, 2, 4, 0.0F, false));
		center_peak_4.cubeList.add(new ModelBox(center_peak_4, 162, 306, -6.0F, -18.2222F, 22.5333F, 12, 8, 8, 0.0F, false));
		center_peak_4.cubeList.add(new ModelBox(center_peak_4, 162, 306, -6.0F, -18.2222F, 30.5333F, 12, 4, 4, 0.0F, false));
		center_peak_4.cubeList.add(new ModelBox(center_peak_4, 162, 306, -12.0F, -12.2222F, -1.0667F, 4, 8, 8, 0.0F, false));
		center_peak_4.cubeList.add(new ModelBox(center_peak_4, 162, 306, -2.0F, -12.2222F, -1.0667F, 4, 8, 8, 0.0F, false));
		center_peak_4.cubeList.add(new ModelBox(center_peak_4, 162, 306, 8.0F, -12.2222F, -1.0667F, 4, 8, 8, 0.0F, false));
		center_peak_4.cubeList.add(new ModelBox(center_peak_4, 162, 306, -16.0F, -13.8222F, 2.5333F, 32, 2, 4, 0.0F, false));
		center_peak_4.cubeList.add(new ModelBox(center_peak_4, 114, 313, -14.0F, -13.8222F, 6.5333F, 28, 2, 4, 0.0F, false));
		center_peak_4.cubeList.add(new ModelBox(center_peak_4, 162, 306, -12.0F, -13.8222F, 10.5333F, 24, 2, 4, 0.0F, false));
		center_peak_4.cubeList.add(new ModelBox(center_peak_4, 185, 288, -10.0F, -13.8222F, 14.5333F, 20, 2, 4, 0.0F, false));
		center_peak_4.cubeList.add(new ModelBox(center_peak_4, 162, 306, -8.0F, -13.8222F, 18.5333F, 16, 2, 4, 0.0F, false));

		wroughtiron_front_4 = new RendererModel(this);
		wroughtiron_front_4.setRotationPoint(0.0F, 191.7778F, 46.3333F);
		roof_side_4.addChild(wroughtiron_front_4);
		wroughtiron_front_4.cubeList.add(new ModelBox(wroughtiron_front_4, 6, 420, -32.0F, -220.0F, -54.0F, 64, 24, 0, 0.0F, false));

		outer_peak_4 = new RendererModel(this);
		outer_peak_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(outer_peak_4, 0.0873F, 0.0F, 0.0F);
		roof_side_4.addChild(outer_peak_4);
		outer_peak_4.cubeList.add(new ModelBox(outer_peak_4, 139, 320, -32.0F, -5.0222F, -10.6667F, 64, 2, 7, 0.0F, false));
		outer_peak_4.cubeList.add(new ModelBox(outer_peak_4, 139, 320, -29.8F, -3.0222F, -7.2667F, 60, 2, 8, 0.0F, false));
		outer_peak_4.cubeList.add(new ModelBox(outer_peak_4, 139, 320, -25.6F, -5.0222F, 0.3333F, 52, 4, 4, 0.0F, false));
		outer_peak_4.cubeList.add(new ModelBox(outer_peak_4, 139, 320, -23.6F, -5.0222F, 4.3333F, 48, 4, 4, 0.0F, false));
		outer_peak_4.cubeList.add(new ModelBox(outer_peak_4, 139, 320, -21.6F, -5.0222F, 8.3333F, 44, 4, 4, 0.0F, false));
		outer_peak_4.cubeList.add(new ModelBox(outer_peak_4, 139, 320, -19.6F, -5.0222F, 12.3333F, 40, 4, 4, 0.0F, false));
		outer_peak_4.cubeList.add(new ModelBox(outer_peak_4, 139, 320, -4.0F, -24.0222F, 42.3333F, 8, 20, 4, 0.0F, false));
		outer_peak_4.cubeList.add(new ModelBox(outer_peak_4, 139, 320, -27.6F, -5.0222F, -3.6667F, 56, 4, 4, 0.0F, false));

		side_4 = new RendererModel(this);
		side_4.setRotationPoint(0.0F, 3.7778F, -1.6667F);
		panel_side_4.addChild(side_4);
		side_4.cubeList.add(new ModelBox(side_4, 146, 393, -28.0F, -36.0F, -49.0F, 56, 4, 4, 0.0F, false));
		side_4.cubeList.add(new ModelBox(side_4, 139, 249, -28.0F, -184.0F, -49.0F, 4, 148, 4, 0.0F, false));
		side_4.cubeList.add(new ModelBox(side_4, 144, 246, 24.0F, -184.0F, -49.0F, 4, 148, 4, 0.0F, false));
		side_4.cubeList.add(new ModelBox(side_4, 235, 246, -28.0F, -192.0F, -49.0F, 56, 8, 4, 0.0F, false));
		side_4.cubeList.add(new ModelBox(side_4, 171, 352, -24.0F, -136.0F, -49.0F, 48, 12, 4, 0.0F, false));
		side_4.cubeList.add(new ModelBox(side_4, 278, 374, -24.0F, -80.0F, -49.0F, 48, 20, 4, 0.0F, false));
		side_4.cubeList.add(new ModelBox(side_4, 118, 267, -28.0F, -195.2F, -51.0F, 56, 4, 4, 0.0F, false));
		side_4.cubeList.add(new ModelBox(side_4, 452, 18, -28.0F, -132.0F, -50.6F, 56, 4, 6, 0.0F, false));
		side_4.cubeList.add(new ModelBox(side_4, 448, 13, -28.0F, -76.0F, -50.6F, 56, 4, 6, 0.0F, false));
		side_4.cubeList.add(new ModelBox(side_4, 448, 84, -28.0F, -66.0F, -50.6F, 56, 4, 6, 0.0F, false));
		side_4.cubeList.add(new ModelBox(side_4, 459, 6, -28.0F, -33.0F, -50.6F, 56, 4, 6, 0.0F, false));
		side_4.cubeList.add(new ModelBox(side_4, 441, 93, -28.0F, -16.0F, -50.6F, 56, 4, 6, 0.0F, false));
		side_4.cubeList.add(new ModelBox(side_4, 288, 422, -28.0F, -32.0F, -49.0F, 56, 16, 4, 0.0F, false));
		side_4.cubeList.add(new ModelBox(side_4, 140, 392, -28.0F, -12.0F, -49.0F, 56, 8, 4, 0.0F, false));

		top_panel_4 = new RendererModel(this);
		top_panel_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_4.addChild(top_panel_4);
		top_panel_4.cubeList.add(new ModelBox(top_panel_4, 262, 80, -24.0F, -180.2222F, -49.6667F, 48, 48, 4, 0.0F, false));

		cage4 = new RendererModel(this);
		cage4.setRotationPoint(0.0F, 3.7778F, -1.6667F);
		top_panel_4.addChild(cage4);
		cage4.cubeList.add(new ModelBox(cage4, 480, 38, -9.0F, -177.0F, -58.0F, 4, 12, 17, 0.0F, false));
		cage4.cubeList.add(new ModelBox(cage4, 480, 38, 5.0F, -177.0F, -58.0F, 4, 8, 17, 0.0F, false));
		cage4.cubeList.add(new ModelBox(cage4, 480, 38, -17.0F, -169.0F, -58.6F, 11, 4, 18, 0.0F, false));
		cage4.cubeList.add(new ModelBox(cage4, 480, 38, -17.0F, -155.0F, -58.6F, 11, 4, 18, 0.0F, false));
		cage4.cubeList.add(new ModelBox(cage4, 480, 38, 5.0F, -151.0F, -58.0F, 4, 8, 17, 0.0F, false));
		cage4.cubeList.add(new ModelBox(cage4, 480, 38, -9.0F, -151.0F, -58.0F, 4, 8, 17, 0.0F, false));
		cage4.cubeList.add(new ModelBox(cage4, 480, 38, 6.0F, -155.0F, -58.6F, 11, 4, 18, 0.0F, false));
		cage4.cubeList.add(new ModelBox(cage4, 480, 38, 6.0F, -169.0F, -58.6F, 11, 4, 18, 0.0F, false));
		cage4.cubeList.add(new ModelBox(cage4, 480, 38, -6.0F, -169.0F, -59.6F, 12, 4, 19, 0.0F, false));
		cage4.cubeList.add(new ModelBox(cage4, 480, 38, -6.0F, -155.0F, -59.6F, 12, 4, 19, 0.0F, false));
		cage4.cubeList.add(new ModelBox(cage4, 480, 38, -9.0F, -165.0F, -58.0F, 4, 12, 17, 0.0F, false));
		cage4.cubeList.add(new ModelBox(cage4, 480, 38, 5.0F, -165.0F, -58.0F, 4, 12, 17, 0.0F, false));
		cage4.cubeList.add(new ModelBox(cage4, 480, 38, -16.0F, -176.0F, -52.0F, 32, 32, 10, 0.0F, false));

		middle_panel_4 = new RendererModel(this);
		middle_panel_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_4.addChild(middle_panel_4);
		middle_panel_4.cubeList.add(new ModelBox(middle_panel_4, 272, 114, -24.0F, -120.2222F, -49.6667F, 48, 44, 4, 0.0F, false));
		middle_panel_4.cubeList.add(new ModelBox(middle_panel_4, 452, 38, -18.0F, -114.2222F, -51.6667F, 36, 32, 4, 0.0F, false));
		middle_panel_4.cubeList.add(new ModelBox(middle_panel_4, 452, 38, 13.0F, -113.2222F, -52.6667F, 4, 4, 4, 0.0F, false));
		middle_panel_4.cubeList.add(new ModelBox(middle_panel_4, 452, 38, -17.0F, -113.2222F, -52.6667F, 4, 4, 4, 0.0F, false));
		middle_panel_4.cubeList.add(new ModelBox(middle_panel_4, 452, 38, -17.0F, -87.2222F, -52.6667F, 4, 4, 4, 0.0F, false));
		middle_panel_4.cubeList.add(new ModelBox(middle_panel_4, 452, 38, 13.0F, -87.2222F, -52.6667F, 4, 4, 4, 0.0F, false));

		lower_panel_4 = new RendererModel(this);
		lower_panel_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_4.addChild(lower_panel_4);

		gear4 = new RendererModel(this);
		gear4.setRotationPoint(6.8F, -42.4222F, -45.8334F);
		setRotationAngle(gear4, 0.0F, 0.0F, -0.6109F);
		lower_panel_4.addChild(gear4);
		gear4.cubeList.add(new ModelBox(gear4, 544, 34, -10.8F, -22.2F, -0.8333F, 2, 4, 4, 0.0F, false));
		gear4.cubeList.add(new ModelBox(gear4, 544, 34, -18.8F, -24.2F, -0.8333F, 8, 8, 4, 0.0F, false));
		gear4.cubeList.add(new ModelBox(gear4, 544, 34, -16.8F, -26.2F, -0.8333F, 4, 2, 4, 0.0F, false));
		gear4.cubeList.add(new ModelBox(gear4, 544, 34, -20.8F, -22.2F, -0.8333F, 2, 4, 4, 0.0F, false));
		gear4.cubeList.add(new ModelBox(gear4, 544, 34, -16.8F, -16.2F, -0.8333F, 4, 2, 4, 0.0F, false));
		gear4.cubeList.add(new ModelBox(gear4, 678, 48, -15.8F, -21.2F, -1.8333F, 2, 2, 4, 0.0F, false));
		gear4.cubeList.add(new ModelBox(gear4, 544, 34, -23.4F, -17.6F, -0.8333F, 4, 4, 4, 0.0F, false));
		gear4.cubeList.add(new ModelBox(gear4, 706, 68, -22.2F, -16.6F, -1.8333F, 2, 2, 4, 0.0F, false));

		componets4 = new RendererModel(this);
		componets4.setRotationPoint(0.0F, 0.0F, 0.0F);
		lower_panel_4.addChild(componets4);
		componets4.cubeList.add(new ModelBox(componets4, 486, 278, -8.0F, -53.2222F, -48.6667F, 12, 8, 4, 0.0F, false));
		componets4.cubeList.add(new ModelBox(componets4, 544, 34, -4.0F, -45.2222F, -46.6667F, 4, 6, 3, 0.0F, false));
		componets4.cubeList.add(new ModelBox(componets4, 486, 267, -14.0F, -42.2222F, -47.2667F, 8, 8, 4, 0.0F, false));
		componets4.cubeList.add(new ModelBox(componets4, 544, 34, -24.0F, -39.2222F, -46.6667F, 24, 4, 3, 0.0F, false));
		componets4.cubeList.add(new ModelBox(componets4, 708, 139, 11.0F, -42.2222F, -47.2667F, 2, 8, 4, 0.0F, false));
		componets4.cubeList.add(new ModelBox(componets4, 496, 274, 15.2F, -38.2222F, -46.0667F, 4, 4, 2, 0.0F, false));
		componets4.cubeList.add(new ModelBox(componets4, 544, 34, 16.2F, -58.2222F, -46.0667F, 2, 20, 2, 0.0F, false));
		componets4.cubeList.add(new ModelBox(componets4, 544, 34, 10.2F, -48.2222F, -46.0667F, 6, 2, 2, 0.0F, false));
		componets4.cubeList.add(new ModelBox(componets4, 480, 281, 6.2F, -49.2222F, -46.0667F, 4, 4, 2, 0.0F, false));
		componets4.cubeList.add(new ModelBox(componets4, 699, 162, 8.0F, -42.2222F, -47.2667F, 2, 8, 4, 0.0F, false));
		componets4.cubeList.add(new ModelBox(componets4, 694, 178, 5.0F, -42.2222F, -47.2667F, 2, 8, 4, 0.0F, false));

		panelbox7 = new RendererModel(this);
		panelbox7.setRotationPoint(0.0F, 0.0F, 0.0F);
		lower_panel_4.addChild(panelbox7);
		panelbox7.cubeList.add(new ModelBox(panelbox7, 651, 45, -24.0F, -57.2222F, -45.2667F, 48, 27, 0, 0.0F, false));
		panelbox7.cubeList.add(new ModelBox(panelbox7, 646, 36, -23.0F, -57.2222F, -49.2667F, 46, 2, 4, 0.0F, false));
		panelbox7.cubeList.add(new ModelBox(panelbox7, 656, 45, -23.0F, -33.2222F, -49.2667F, 46, 2, 4, 0.0F, false));

		panelbox8 = new RendererModel(this);
		panelbox8.setRotationPoint(0.0F, 0.0F, 0.0F);
		lower_panel_4.addChild(panelbox8);
		panelbox8.cubeList.add(new ModelBox(panelbox8, 708, 36, -25.0F, -57.2222F, -49.2667F, 2, 26, 4, 0.0F, false));
		panelbox8.cubeList.add(new ModelBox(panelbox8, 692, 29, 23.0F, -57.2222F, -49.2667F, 2, 26, 4, 0.0F, false));

		floorboards_4 = new RendererModel(this);
		floorboards_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_4.addChild(floorboards_4);
		floorboards_4.cubeList.add(new ModelBox(floorboards_4, 246, 173, -32.4F, -0.2222F, -58.6667F, 64, 4, 4, 0.0F, false));
		floorboards_4.cubeList.add(new ModelBox(floorboards_4, 246, 173, -26.0F, -0.2222F, -46.6667F, 52, 4, 4, 0.0F, false));
		floorboards_4.cubeList.add(new ModelBox(floorboards_4, 246, 173, -24.0F, -0.2222F, -42.6667F, 48, 4, 4, 0.0F, false));
		floorboards_4.cubeList.add(new ModelBox(floorboards_4, 246, 173, -22.0F, -0.2222F, -38.6667F, 44, 4, 4, 0.0F, false));
		floorboards_4.cubeList.add(new ModelBox(floorboards_4, 246, 173, -18.0F, -0.2222F, -34.6667F, 36, 4, 4, 0.0F, false));
		floorboards_4.cubeList.add(new ModelBox(floorboards_4, 246, 173, -16.0F, -0.2222F, -30.6667F, 32, 4, 4, 0.0F, false));
		floorboards_4.cubeList.add(new ModelBox(floorboards_4, 246, 173, -14.0F, -0.2222F, -26.6667F, 28, 4, 4, 0.0F, false));
		floorboards_4.cubeList.add(new ModelBox(floorboards_4, 246, 173, -12.0F, -0.2222F, -22.6667F, 24, 4, 4, 0.0F, false));
		floorboards_4.cubeList.add(new ModelBox(floorboards_4, 246, 173, -10.0F, -0.2222F, -18.6667F, 20, 4, 4, 0.0F, false));
		floorboards_4.cubeList.add(new ModelBox(floorboards_4, 246, 173, -8.0F, -0.2222F, -14.6667F, 16, 4, 4, 0.0F, false));
		floorboards_4.cubeList.add(new ModelBox(floorboards_4, 246, 173, -6.0F, -0.2222F, -10.6667F, 12, 4, 4, 0.0F, false));
		floorboards_4.cubeList.add(new ModelBox(floorboards_4, 246, 173, -4.0F, -0.2222F, -6.6667F, 8, 4, 4, 0.0F, false));
		floorboards_4.cubeList.add(new ModelBox(floorboards_4, 246, 173, -30.8F, -0.2222F, -54.6667F, 60, 4, 4, 0.0F, false));
		floorboards_4.cubeList.add(new ModelBox(floorboards_4, 246, 173, -28.0F, -0.2222F, -50.6667F, 56, 4, 4, 0.0F, false));

		panel_side_5 = new RendererModel(this);
		panel_side_5.setRotationPoint(0.0F, -3.7778F, 1.6667F);
		setRotationAngle(panel_side_5, 0.0F, 1.0472F, 0.0F);
		box.addChild(panel_side_5);

		roof_side_5 = new RendererModel(this);
		roof_side_5.setRotationPoint(0.0F, -188.0F, -48.0F);
		panel_side_5.addChild(roof_side_5);

		center_peak_5 = new RendererModel(this);
		center_peak_5.setRotationPoint(0.0F, 0.0F, 16.0F);
		setRotationAngle(center_peak_5, 0.1047F, 0.0F, 0.0F);
		roof_side_5.addChild(center_peak_5);
		center_peak_5.cubeList.add(new ModelBox(center_peak_5, 196, 306, -18.0F, -13.8222F, -1.4667F, 37, 2, 4, 0.0F, false));
		center_peak_5.cubeList.add(new ModelBox(center_peak_5, 162, 306, -6.0F, -18.2222F, 22.5333F, 12, 8, 8, 0.0F, false));
		center_peak_5.cubeList.add(new ModelBox(center_peak_5, 162, 306, -6.0F, -18.2222F, 30.5333F, 12, 4, 4, 0.0F, false));
		center_peak_5.cubeList.add(new ModelBox(center_peak_5, 162, 306, -12.0F, -12.2222F, -1.0667F, 4, 8, 8, 0.0F, false));
		center_peak_5.cubeList.add(new ModelBox(center_peak_5, 162, 306, -2.0F, -12.2222F, -1.0667F, 4, 8, 8, 0.0F, false));
		center_peak_5.cubeList.add(new ModelBox(center_peak_5, 162, 306, 8.0F, -12.2222F, -1.0667F, 4, 8, 8, 0.0F, false));
		center_peak_5.cubeList.add(new ModelBox(center_peak_5, 162, 306, -16.0F, -13.8222F, 2.5333F, 32, 2, 4, 0.0F, false));
		center_peak_5.cubeList.add(new ModelBox(center_peak_5, 114, 313, -14.0F, -13.8222F, 6.5333F, 28, 2, 4, 0.0F, false));
		center_peak_5.cubeList.add(new ModelBox(center_peak_5, 162, 306, -12.0F, -13.8222F, 10.5333F, 24, 2, 4, 0.0F, false));
		center_peak_5.cubeList.add(new ModelBox(center_peak_5, 185, 288, -10.0F, -13.8222F, 14.5333F, 20, 2, 4, 0.0F, false));
		center_peak_5.cubeList.add(new ModelBox(center_peak_5, 162, 306, -8.0F, -13.8222F, 18.5333F, 16, 2, 4, 0.0F, false));

		wroughtiron_front_5 = new RendererModel(this);
		wroughtiron_front_5.setRotationPoint(0.0F, 191.7778F, 46.3333F);
		roof_side_5.addChild(wroughtiron_front_5);
		wroughtiron_front_5.cubeList.add(new ModelBox(wroughtiron_front_5, 6, 420, -32.0F, -220.0F, -54.0F, 64, 24, 0, 0.0F, false));

		outer_peak_5 = new RendererModel(this);
		outer_peak_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(outer_peak_5, 0.0873F, 0.0F, 0.0F);
		roof_side_5.addChild(outer_peak_5);
		outer_peak_5.cubeList.add(new ModelBox(outer_peak_5, 139, 320, -32.0F, -5.0222F, -10.6667F, 64, 2, 7, 0.0F, false));
		outer_peak_5.cubeList.add(new ModelBox(outer_peak_5, 139, 320, -29.8F, -3.0222F, -7.2667F, 60, 2, 8, 0.0F, false));
		outer_peak_5.cubeList.add(new ModelBox(outer_peak_5, 139, 320, -25.6F, -5.0222F, 0.3333F, 52, 4, 4, 0.0F, false));
		outer_peak_5.cubeList.add(new ModelBox(outer_peak_5, 139, 320, -23.6F, -5.0222F, 4.3333F, 48, 4, 4, 0.0F, false));
		outer_peak_5.cubeList.add(new ModelBox(outer_peak_5, 139, 320, -21.6F, -5.0222F, 8.3333F, 44, 4, 4, 0.0F, false));
		outer_peak_5.cubeList.add(new ModelBox(outer_peak_5, 139, 320, -19.6F, -5.0222F, 12.3333F, 40, 4, 4, 0.0F, false));
		outer_peak_5.cubeList.add(new ModelBox(outer_peak_5, 139, 320, -4.0F, -24.0222F, 42.3333F, 8, 20, 4, 0.0F, false));
		outer_peak_5.cubeList.add(new ModelBox(outer_peak_5, 139, 320, -27.6F, -5.0222F, -3.6667F, 56, 4, 4, 0.0F, false));

		side_5 = new RendererModel(this);
		side_5.setRotationPoint(0.0F, 3.7778F, -1.6667F);
		panel_side_5.addChild(side_5);
		side_5.cubeList.add(new ModelBox(side_5, 146, 393, -28.0F, -36.0F, -49.0F, 56, 4, 4, 0.0F, false));
		side_5.cubeList.add(new ModelBox(side_5, 139, 249, -28.0F, -184.0F, -49.0F, 4, 148, 4, 0.0F, false));
		side_5.cubeList.add(new ModelBox(side_5, 144, 246, 24.0F, -184.0F, -49.0F, 4, 148, 4, 0.0F, false));
		side_5.cubeList.add(new ModelBox(side_5, 235, 246, -28.0F, -192.0F, -49.0F, 56, 8, 4, 0.0F, false));
		side_5.cubeList.add(new ModelBox(side_5, 171, 352, -24.0F, -136.0F, -49.0F, 48, 12, 4, 0.0F, false));
		side_5.cubeList.add(new ModelBox(side_5, 278, 374, -24.0F, -80.0F, -49.0F, 48, 20, 4, 0.0F, false));
		side_5.cubeList.add(new ModelBox(side_5, 118, 267, -28.0F, -195.2F, -51.0F, 56, 4, 4, 0.0F, false));
		side_5.cubeList.add(new ModelBox(side_5, 452, 18, -28.0F, -132.0F, -50.6F, 56, 4, 6, 0.0F, false));
		side_5.cubeList.add(new ModelBox(side_5, 448, 13, -28.0F, -76.0F, -50.6F, 56, 4, 6, 0.0F, false));
		side_5.cubeList.add(new ModelBox(side_5, 448, 84, -28.0F, -66.0F, -50.6F, 56, 4, 6, 0.0F, false));
		side_5.cubeList.add(new ModelBox(side_5, 459, 6, -28.0F, -33.0F, -50.6F, 56, 4, 6, 0.0F, false));
		side_5.cubeList.add(new ModelBox(side_5, 441, 93, -28.0F, -16.0F, -50.6F, 56, 4, 6, 0.0F, false));
		side_5.cubeList.add(new ModelBox(side_5, 288, 422, -28.0F, -32.0F, -49.0F, 56, 16, 4, 0.0F, false));
		side_5.cubeList.add(new ModelBox(side_5, 140, 392, -28.0F, -12.0F, -49.0F, 56, 8, 4, 0.0F, false));

		top_panel_5 = new RendererModel(this);
		top_panel_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_5.addChild(top_panel_5);
		top_panel_5.cubeList.add(new ModelBox(top_panel_5, 262, 96, -24.0F, -180.2222F, -49.6667F, 48, 48, 4, 0.0F, false));

		cage5 = new RendererModel(this);
		cage5.setRotationPoint(0.0F, 3.7778F, -1.6667F);
		top_panel_5.addChild(cage5);
		cage5.cubeList.add(new ModelBox(cage5, 480, 38, -9.0F, -177.0F, -58.0F, 4, 12, 17, 0.0F, false));
		cage5.cubeList.add(new ModelBox(cage5, 480, 38, 5.0F, -177.0F, -58.0F, 4, 8, 17, 0.0F, false));
		cage5.cubeList.add(new ModelBox(cage5, 480, 38, -17.0F, -169.0F, -58.6F, 11, 4, 18, 0.0F, false));
		cage5.cubeList.add(new ModelBox(cage5, 480, 38, -17.0F, -155.0F, -58.6F, 11, 4, 18, 0.0F, false));
		cage5.cubeList.add(new ModelBox(cage5, 480, 38, 5.0F, -151.0F, -58.0F, 4, 8, 17, 0.0F, false));
		cage5.cubeList.add(new ModelBox(cage5, 480, 38, -9.0F, -151.0F, -58.0F, 4, 8, 17, 0.0F, false));
		cage5.cubeList.add(new ModelBox(cage5, 480, 38, 6.0F, -155.0F, -58.6F, 11, 4, 18, 0.0F, false));
		cage5.cubeList.add(new ModelBox(cage5, 480, 38, 6.0F, -169.0F, -58.6F, 11, 4, 18, 0.0F, false));
		cage5.cubeList.add(new ModelBox(cage5, 480, 38, -6.0F, -169.0F, -59.6F, 12, 4, 19, 0.0F, false));
		cage5.cubeList.add(new ModelBox(cage5, 480, 38, -6.0F, -155.0F, -59.6F, 12, 4, 19, 0.0F, false));
		cage5.cubeList.add(new ModelBox(cage5, 480, 38, -9.0F, -165.0F, -58.0F, 4, 12, 17, 0.0F, false));
		cage5.cubeList.add(new ModelBox(cage5, 480, 38, 5.0F, -165.0F, -58.0F, 4, 12, 17, 0.0F, false));
		cage5.cubeList.add(new ModelBox(cage5, 480, 38, -16.0F, -176.0F, -52.0F, 32, 32, 10, 0.0F, false));

		middle_panel_5 = new RendererModel(this);
		middle_panel_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_5.addChild(middle_panel_5);
		middle_panel_5.cubeList.add(new ModelBox(middle_panel_5, 256, 105, -24.0F, -120.2222F, -49.6667F, 48, 44, 4, 0.0F, false));
		middle_panel_5.cubeList.add(new ModelBox(middle_panel_5, 452, 38, -18.0F, -114.2222F, -51.6667F, 36, 32, 4, 0.0F, false));
		middle_panel_5.cubeList.add(new ModelBox(middle_panel_5, 452, 38, 13.0F, -113.2222F, -52.6667F, 4, 4, 4, 0.0F, false));
		middle_panel_5.cubeList.add(new ModelBox(middle_panel_5, 452, 38, -17.0F, -113.2222F, -52.6667F, 4, 4, 4, 0.0F, false));
		middle_panel_5.cubeList.add(new ModelBox(middle_panel_5, 452, 38, -17.0F, -87.2222F, -52.6667F, 4, 4, 4, 0.0F, false));
		middle_panel_5.cubeList.add(new ModelBox(middle_panel_5, 452, 38, 13.0F, -87.2222F, -52.6667F, 4, 4, 4, 0.0F, false));

		lower_panel_5 = new RendererModel(this);
		lower_panel_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_5.addChild(lower_panel_5);

		componets5 = new RendererModel(this);
		componets5.setRotationPoint(0.0F, 0.0F, 0.0F);
		lower_panel_5.addChild(componets5);
		componets5.cubeList.add(new ModelBox(componets5, 505, 64, 16.4F, -43.2222F, -46.0667F, 2, 2, 2, 0.0F, false));
		componets5.cubeList.add(new ModelBox(componets5, 505, 64, 19.4F, -53.2222F, -46.0667F, 2, 2, 2, 0.0F, false));
		componets5.cubeList.add(new ModelBox(componets5, 505, 64, -4.0F, -46.4222F, -48.6667F, 6, 12, 4, 0.0F, false));
		componets5.cubeList.add(new ModelBox(componets5, 564, 148, 16.2F, -53.2222F, -46.0667F, 2, 2, 2, 0.0F, false));
		componets5.cubeList.add(new ModelBox(componets5, 505, 64, 19.4F, -50.2222F, -46.0667F, 2, 2, 2, 0.0F, false));
		componets5.cubeList.add(new ModelBox(componets5, 505, 64, 16.2F, -50.2222F, -46.0667F, 2, 2, 2, 0.0F, false));
		componets5.cubeList.add(new ModelBox(componets5, 466, 144, 16.4F, -40.2222F, -46.0667F, 2, 2, 2, 0.0F, false));
		componets5.cubeList.add(new ModelBox(componets5, 505, 64, 16.4F, -37.2222F, -46.0667F, 2, 2, 2, 0.0F, false));
		componets5.cubeList.add(new ModelBox(componets5, 505, 64, 16.2F, -47.2222F, -46.0667F, 2, 2, 2, 0.0F, false));
		componets5.cubeList.add(new ModelBox(componets5, 498, 203, 19.4F, -47.2222F, -46.0667F, 2, 2, 2, 0.0F, false));
		componets5.cubeList.add(new ModelBox(componets5, 500, 278, -2.8F, -58.2222F, -47.6667F, 4, 12, 2, 0.0F, false));
		componets5.cubeList.add(new ModelBox(componets5, 514, 180, 19.4F, -43.2222F, -46.0667F, 2, 2, 2, 0.0F, false));
		componets5.cubeList.add(new ModelBox(componets5, 505, 64, 19.4F, -40.2222F, -46.0667F, 2, 2, 2, 0.0F, false));
		componets5.cubeList.add(new ModelBox(componets5, 505, 64, 19.4F, -37.2222F, -46.0667F, 2, 2, 2, 0.0F, false));
		componets5.cubeList.add(new ModelBox(componets5, 489, 276, -10.4F, -58.2222F, -47.6667F, 4, 12, 2, 0.0F, false));
		componets5.cubeList.add(new ModelBox(componets5, 505, 64, -11.2F, -46.4222F, -48.6667F, 6, 12, 4, 0.0F, false));
		componets5.cubeList.add(new ModelBox(componets5, 493, 288, -17.6F, -58.2222F, -47.6667F, 4, 12, 2, 0.0F, false));
		componets5.cubeList.add(new ModelBox(componets5, 505, 64, -18.4F, -46.4222F, -48.6667F, 6, 12, 4, 0.0F, false));
		componets5.cubeList.add(new ModelBox(componets5, 505, 64, -4.0F, -58.4222F, -48.6667F, 6, 4, 4, 0.0F, false));
		componets5.cubeList.add(new ModelBox(componets5, 505, 64, -11.6F, -58.4222F, -48.6667F, 6, 4, 4, 0.0F, false));
		componets5.cubeList.add(new ModelBox(componets5, 505, 64, -18.8F, -58.4222F, -48.6667F, 6, 4, 4, 0.0F, false));
		componets5.cubeList.add(new ModelBox(componets5, 672, 166, -2.8F, -44.6222F, -49.6667F, 4, 8, 2, 0.0F, false));
		componets5.cubeList.add(new ModelBox(componets5, 676, 180, -10.0F, -44.6222F, -49.6667F, 4, 8, 2, 0.0F, false));
		componets5.cubeList.add(new ModelBox(componets5, 683, 160, -17.2F, -44.6222F, -49.6667F, 4, 8, 2, 0.0F, false));
		componets5.cubeList.add(new ModelBox(componets5, 505, 64, 5.2F, -58.2222F, -47.6667F, 2, 28, 2, 0.0F, false));
		componets5.cubeList.add(new ModelBox(componets5, 505, 64, 9.2F, -58.2222F, -47.6667F, 2, 28, 2, 0.0F, false));

		panelbox9 = new RendererModel(this);
		panelbox9.setRotationPoint(0.0F, 0.0F, 0.0F);
		lower_panel_5.addChild(panelbox9);
		panelbox9.cubeList.add(new ModelBox(panelbox9, 665, 36, -24.0F, -57.2222F, -45.2667F, 48, 27, 0, 0.0F, false));
		panelbox9.cubeList.add(new ModelBox(panelbox9, 646, 57, -23.0F, -57.2222F, -49.2667F, 46, 2, 4, 0.0F, false));
		panelbox9.cubeList.add(new ModelBox(panelbox9, 660, 66, -23.0F, -33.2222F, -49.2667F, 46, 2, 4, 0.0F, false));

		panelbox10 = new RendererModel(this);
		panelbox10.setRotationPoint(0.0F, 0.0F, 0.0F);
		lower_panel_5.addChild(panelbox10);
		panelbox10.cubeList.add(new ModelBox(panelbox10, 688, 45, -25.0F, -57.2222F, -49.2667F, 2, 26, 4, 0.0F, false));
		panelbox10.cubeList.add(new ModelBox(panelbox10, 692, 48, 23.0F, -57.2222F, -49.2667F, 2, 26, 4, 0.0F, false));

		floorboards_5 = new RendererModel(this);
		floorboards_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_side_5.addChild(floorboards_5);
		floorboards_5.cubeList.add(new ModelBox(floorboards_5, 249, 171, -32.4F, -0.2222F, -58.6667F, 64, 4, 4, 0.0F, false));
		floorboards_5.cubeList.add(new ModelBox(floorboards_5, 249, 171, -26.0F, -0.2222F, -46.6667F, 52, 4, 4, 0.0F, false));
		floorboards_5.cubeList.add(new ModelBox(floorboards_5, 249, 171, -24.0F, -0.2222F, -42.6667F, 48, 4, 4, 0.0F, false));
		floorboards_5.cubeList.add(new ModelBox(floorboards_5, 249, 171, -22.0F, -0.2222F, -38.6667F, 44, 4, 4, 0.0F, false));
		floorboards_5.cubeList.add(new ModelBox(floorboards_5, 249, 171, -18.0F, -0.2222F, -34.6667F, 36, 4, 4, 0.0F, false));
		floorboards_5.cubeList.add(new ModelBox(floorboards_5, 249, 171, -16.0F, -0.2222F, -30.6667F, 32, 4, 4, 0.0F, false));
		floorboards_5.cubeList.add(new ModelBox(floorboards_5, 249, 171, -14.0F, -0.2222F, -26.6667F, 28, 4, 4, 0.0F, false));
		floorboards_5.cubeList.add(new ModelBox(floorboards_5, 249, 171, -12.0F, -0.2222F, -22.6667F, 24, 4, 4, 0.0F, false));
		floorboards_5.cubeList.add(new ModelBox(floorboards_5, 249, 171, -10.0F, -0.2222F, -18.6667F, 20, 4, 4, 0.0F, false));
		floorboards_5.cubeList.add(new ModelBox(floorboards_5, 249, 171, -8.0F, -0.2222F, -14.6667F, 16, 4, 4, 0.0F, false));
		floorboards_5.cubeList.add(new ModelBox(floorboards_5, 249, 171, -6.0F, -0.2222F, -10.6667F, 12, 4, 4, 0.0F, false));
		floorboards_5.cubeList.add(new ModelBox(floorboards_5, 249, 171, -4.0F, -0.2222F, -6.6667F, 8, 4, 4, 0.0F, false));
		floorboards_5.cubeList.add(new ModelBox(floorboards_5, 249, 171, -30.8F, -0.2222F, -54.6667F, 60, 4, 4, 0.0F, false));
		floorboards_5.cubeList.add(new ModelBox(floorboards_5, 249, 171, -28.0F, -0.2222F, -50.6667F, 56, 4, 4, 0.0F, false));

		hexspine = new RendererModel(this);
		hexspine.setRotationPoint(0.0F, -2.0F, 2.0F);
		box.addChild(hexspine);

		crossspine = new RendererModel(this);
		crossspine.setRotationPoint(0.0F, 0.0F, 0.0F);
		hexspine.addChild(crossspine);
		crossspine.cubeList.add(new ModelBox(crossspine, 59, 398, -68.0F, -3.0F, -2.0F, 136, 6, 4, 0.0F, false));
		crossspine.cubeList.add(new ModelBox(crossspine, 239, 212, -62.0F, -190.0F, -2.0F, 8, 188, 4, 0.0F, false));
		crossspine.cubeList.add(new ModelBox(crossspine, 161, 218, 54.0F, -190.0F, -2.0F, 8, 188, 4, 0.0F, false));
		crossspine.cubeList.add(new ModelBox(crossspine, 491, 304, -65.0F, -218.0F, -2.0F, 4, 24, 4, 0.0F, false));
		crossspine.cubeList.add(new ModelBox(crossspine, 466, 294, 61.0F, -218.0F, -2.0F, 4, 24, 4, 0.0F, false));

		leftspine = new RendererModel(this);
		leftspine.setRotationPoint(0.25F, -151.8125F, 0.0F);
		setRotationAngle(leftspine, 0.0F, 1.0472F, 0.0F);
		hexspine.addChild(leftspine);
		leftspine.cubeList.add(new ModelBox(leftspine, 59, 397, -68.25F, 148.8125F, -2.0F, 136, 6, 4, 0.0F, false));
		leftspine.cubeList.add(new ModelBox(leftspine, 61, 214, 53.75F, -38.1875F, -2.0F, 8, 188, 4, 0.0F, false));
		leftspine.cubeList.add(new ModelBox(leftspine, 330, 218, -62.25F, -38.1875F, -2.0F, 8, 188, 4, 0.0F, false));
		leftspine.cubeList.add(new ModelBox(leftspine, 468, 301, -65.25F, -66.1875F, -2.0F, 4, 24, 4, 0.0F, false));
		leftspine.cubeList.add(new ModelBox(leftspine, 498, 297, 60.75F, -66.1875F, -2.0F, 4, 24, 4, 0.0F, false));

		upperrails = new RendererModel(this);
		upperrails.setRotationPoint(0.0F, 0.0F, 0.0F);
		leftspine.addChild(upperrails);

		upperrail_1 = new RendererModel(this);
		upperrail_1.setRotationPoint(10.0F, -52.0F, 0.0F);
		setRotationAngle(upperrail_1, 0.0F, 0.0F, 0.0873F);
		upperrails.addChild(upperrail_1);
		upperrail_1.cubeList.add(new ModelBox(upperrail_1, 173, 306, -5.85F, -2.5875F, -2.0F, 36, 8, 4, 0.0F, false));
		upperrail_1.cubeList.add(new ModelBox(upperrail_1, 189, 310, -5.85F, 4.8125F, -2.0F, 64, 3, 4, 0.0F, false));
		upperrail_1.cubeList.add(new ModelBox(upperrail_1, 189, 310, -5.85F, 6.8125F, -2.0F, 62, 2, 4, 0.0F, false));
		upperrail_1.cubeList.add(new ModelBox(upperrail_1, 189, 310, -5.85F, 8.8125F, -2.0F, 60, 2, 4, 0.0F, false));
		upperrail_1.cubeList.add(new ModelBox(upperrail_1, 692, 180, -4.85F, -8.5875F, -2.0F, 8, 6, 4, 0.0F, false));
		upperrail_1.cubeList.add(new ModelBox(upperrail_1, 708, 54, 3.15F, -9.5875F, -3.0F, 2, 7, 6, 0.0F, false));

		upperrails2 = new RendererModel(this);
		upperrails2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(upperrails2, 0.0F, 1.0472F, 0.0F);
		leftspine.addChild(upperrails2);

		upperrail_2 = new RendererModel(this);
		upperrail_2.setRotationPoint(10.0F, -52.0F, 0.0F);
		setRotationAngle(upperrail_2, 0.0F, 0.0F, 0.0873F);
		upperrails2.addChild(upperrail_2);
		upperrail_2.cubeList.add(new ModelBox(upperrail_2, 228, 288, -5.85F, -2.5875F, -2.0F, 36, 8, 4, 0.0F, false));
		upperrail_2.cubeList.add(new ModelBox(upperrail_2, 178, 240, -5.85F, 4.8125F, -2.0F, 64, 2, 4, 0.0F, false));
		upperrail_2.cubeList.add(new ModelBox(upperrail_2, 262, 240, -5.85F, 6.8125F, -2.0F, 62, 2, 4, 0.0F, false));
		upperrail_2.cubeList.add(new ModelBox(upperrail_2, 278, 292, -5.85F, 8.8125F, -2.0F, 60, 2, 4, 0.0F, false));
		upperrail_2.cubeList.add(new ModelBox(upperrail_2, 688, 171, -3.85F, -8.5875F, -2.0F, 8, 6, 4, 0.0F, false));
		upperrail_2.cubeList.add(new ModelBox(upperrail_2, 662, 38, 4.15F, -9.5875F, -3.0F, 2, 7, 6, 0.0F, false));

		upperrails3 = new RendererModel(this);
		upperrails3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(upperrails3, 0.0F, 2.0944F, 0.0F);
		leftspine.addChild(upperrails3);

		upperrail_3 = new RendererModel(this);
		upperrail_3.setRotationPoint(10.0F, -52.0F, 0.0F);
		setRotationAngle(upperrail_3, 0.0F, 0.0F, 0.0873F);
		upperrails3.addChild(upperrail_3);
		upperrail_3.cubeList.add(new ModelBox(upperrail_3, 180, 292, -5.85F, -2.5875F, -2.0F, 36, 8, 4, 0.0F, false));
		upperrail_3.cubeList.add(new ModelBox(upperrail_3, 238, 228, -5.85F, 4.8125F, -2.0F, 64, 2, 4, 0.0F, false));
		upperrail_3.cubeList.add(new ModelBox(upperrail_3, 258, 234, -5.85F, 6.8125F, -2.0F, 62, 2, 4, 0.0F, false));
		upperrail_3.cubeList.add(new ModelBox(upperrail_3, 278, 286, -5.85F, 8.8125F, -2.0F, 60, 2, 4, 0.0F, false));
		upperrail_3.cubeList.add(new ModelBox(upperrail_3, 699, 162, -4.85F, -8.5875F, -2.0F, 8, 6, 4, 0.0F, false));
		upperrail_3.cubeList.add(new ModelBox(upperrail_3, 688, 57, 3.15F, -9.5875F, -3.0F, 2, 7, 6, 0.0F, false));

		upperrails4 = new RendererModel(this);
		upperrails4.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(upperrails4, 0.0F, 3.1416F, 0.0F);
		leftspine.addChild(upperrails4);

		upperrail_4 = new RendererModel(this);
		upperrail_4.setRotationPoint(10.0F, -52.0F, 0.0F);
		setRotationAngle(upperrail_4, 0.0F, 0.0F, 0.0873F);
		upperrails4.addChild(upperrail_4);
		upperrail_4.cubeList.add(new ModelBox(upperrail_4, 697, 171, -4.85F, -8.5875F, -2.0F, 8, 6, 4, 0.0F, false));
		upperrail_4.cubeList.add(new ModelBox(upperrail_4, 54, 267, -5.85F, -2.5875F, -2.0F, 36, 8, 4, 0.0F, false));
		upperrail_4.cubeList.add(new ModelBox(upperrail_4, 238, 222, -5.85F, 4.8125F, -2.0F, 64, 2, 4, 0.0F, false));
		upperrail_4.cubeList.add(new ModelBox(upperrail_4, 250, 106, -5.85F, 6.8125F, -2.0F, 62, 2, 4, 0.0F, false));
		upperrail_4.cubeList.add(new ModelBox(upperrail_4, 278, 272, -5.85F, 8.8125F, -2.0F, 60, 2, 4, 0.0F, false));
		upperrail_4.cubeList.add(new ModelBox(upperrail_4, 704, 57, 3.15F, -9.5875F, -3.0F, 2, 7, 6, 0.0F, false));

		upperrails5 = new RendererModel(this);
		upperrails5.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(upperrails5, 0.0F, -2.0944F, 0.0F);
		leftspine.addChild(upperrails5);

		upperrail_5 = new RendererModel(this);
		upperrail_5.setRotationPoint(10.0F, -52.0F, 0.0F);
		setRotationAngle(upperrail_5, 0.0F, 0.0F, 0.0873F);
		upperrails5.addChild(upperrail_5);
		upperrail_5.cubeList.add(new ModelBox(upperrail_5, 690, 182, -4.85F, -8.5875F, -2.0F, 8, 6, 4, 0.0F, false));
		upperrail_5.cubeList.add(new ModelBox(upperrail_5, 59, 301, -5.85F, -2.5875F, -2.0F, 36, 8, 4, 0.0F, false));
		upperrail_5.cubeList.add(new ModelBox(upperrail_5, 238, 216, -5.85F, 4.8125F, -2.0F, 64, 2, 4, 0.0F, false));
		upperrail_5.cubeList.add(new ModelBox(upperrail_5, 250, 100, -5.85F, 6.8125F, -2.0F, 62, 2, 4, 0.0F, false));
		upperrail_5.cubeList.add(new ModelBox(upperrail_5, 278, 266, -5.85F, 8.8125F, -2.0F, 60, 2, 4, 0.0F, false));
		upperrail_5.cubeList.add(new ModelBox(upperrail_5, 692, 48, 3.15F, -9.5875F, -3.0F, 2, 7, 6, 0.0F, false));

		upperrails6 = new RendererModel(this);
		upperrails6.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(upperrails6, 0.0F, -1.0472F, 0.0F);
		leftspine.addChild(upperrails6);

		upperrail_6 = new RendererModel(this);
		upperrail_6.setRotationPoint(10.0F, -52.0F, 0.0F);
		setRotationAngle(upperrail_6, 0.0F, 0.0F, 0.0873F);
		upperrails6.addChild(upperrail_6);
		upperrail_6.cubeList.add(new ModelBox(upperrail_6, 704, 178, -4.85F, -8.5875F, -2.0F, 8, 6, 4, 0.0F, false));
		upperrail_6.cubeList.add(new ModelBox(upperrail_6, 114, 354, -5.85F, -2.5875F, -2.0F, 36, 8, 4, 0.0F, false));
		upperrail_6.cubeList.add(new ModelBox(upperrail_6, 114, 354, -5.85F, 4.8125F, -2.0F, 64, 2, 4, 0.0F, false));
		upperrail_6.cubeList.add(new ModelBox(upperrail_6, 114, 354, -5.85F, 6.8125F, -2.0F, 62, 2, 4, 0.0F, false));
		upperrail_6.cubeList.add(new ModelBox(upperrail_6, 114, 354, -5.85F, 8.8125F, -2.0F, 60, 2, 4, 0.0F, false));
		upperrail_6.cubeList.add(new ModelBox(upperrail_6, 678, 45, 3.15F, -9.5875F, -3.0F, 2, 7, 6, 0.0F, false));

		rightspine = new RendererModel(this);
		rightspine.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(rightspine, 0.0F, -1.0472F, 0.0F);
		hexspine.addChild(rightspine);
		rightspine.cubeList.add(new ModelBox(rightspine, 39, 398, -68.0F, -3.0F, -2.0F, 136, 6, 4, 0.0F, false));
		rightspine.cubeList.add(new ModelBox(rightspine, 14, 214, -62.0F, -190.0F, -2.0F, 8, 188, 4, 0.0F, false));
		rightspine.cubeList.add(new ModelBox(rightspine, 189, 218, 54.0F, -190.0F, -2.0F, 8, 188, 4, 0.0F, false));
		rightspine.cubeList.add(new ModelBox(rightspine, 470, 304, -65.0F, -218.0F, -2.0F, 4, 24, 4, 0.0F, false));
		rightspine.cubeList.add(new ModelBox(rightspine, 560, 306, 61.0F, -218.0F, -2.0F, 4, 24, 4, 0.0F, false));
	}

	public void render(ExteriorTile tile) {
		
		door_rotate_y.rotateAngleY = (float)Math.toRadians(tile.getOpen() == EnumDoorState.ONE ? 55 : 
			(tile.getOpen() == EnumDoorState.BOTH ? 85 : 0));
		
		door_rotate_y.render(ModelHelper.RENDER_SCALE);
		box.render(0.0625F);
		ModelHelper.renderPartBrightness(tile.getLightLevel(), this.glow);
		
	}
	
	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	
	@Override
	public void renderOverwrite(BotiContext context) {
		Minecraft.getInstance().textureManager.bindTexture(SteamExteriorRenderer.TEXTURE);
		if(context instanceof BotiContextExterior) {
			ExteriorTile ext = ((BotiContextExterior)context).getConsole();
			door_rotate_y.rotateAngleY = (float)Math.toRadians(ext.getOpen() == EnumDoorState.ONE ? 55 : 
				(ext.getOpen() == EnumDoorState.BOTH ? 85 : 0));
			door_rotate_y.render(ModelHelper.RENDER_SCALE);
		}
	}
	
	public void renderDoor(double angle, float brightness) {
		door_rotate_y.rotateAngleY = (float)Math.toRadians(angle);
		door_rotate_y.render(ModelHelper.RENDER_SCALE);

		box.render(0.0625F);
		ModelHelper.renderPartBrightness(brightness, this.glow);
	}
	
	public void renderDoor(double angle) {
		this.renderDoor(angle, 1F);
	}

	@Override
	public void renderBoti(float scale, boolean raw) {
		if(raw){
			boti.render(0.0625F);
			return;
		}
		GlStateManager.pushMatrix();
		GlStateManager.disableTexture();
		GlStateManager.color3f(1F, 1F, 1F);
		boti.render(0.0625F);
		GlStateManager.enableTexture();
		GlStateManager.popMatrix();

	}

	@Override
	public void renderEntity(TardisEntity ent) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(0, -0.25, 0);
		GlStateManager.scaled(0.25, 0.25, 0.25);
		Minecraft.getInstance().getTextureManager().bindTexture(SteamExteriorRenderer.TEXTURE);
		door_rotate_y.render(ModelHelper.RENDER_SCALE);
		box.render(0.0625F);
		ModelHelper.renderPartBrightness(1F, this.glow);
		GlStateManager.popMatrix();
	}
}