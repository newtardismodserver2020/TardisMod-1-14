package net.tardis.mod.client.models;

import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;

public class FixedRendererModel extends RendererModel{

	public FixedRendererModel(Model model) {
		super(model);
	}

	public RendererModel addBox(float offX, float offY, float offZ, float width, float height, float depth) {
		return super.addBox(offX, offY, offZ, (int)width, (int)height, (int)depth);
	}

	public RendererModel addBox(float offX, float offY, float offZ, float width, float height, float depth, boolean mirrored) {
		return super.addBox(offX, offY, offZ, (int)width, (int)height, (int)depth, mirrored);
	}

	public void addBox(float offX, float offY, float offZ, float width, float height, float depth, float scaleFactor) {
		super.addBox(offX, offY, offZ, (int)width, (int)height, (int)depth, scaleFactor);
	}

	public void addBox(float offX, float offY, float offZ, float width, float height, float depth, float scaleFactor, boolean mirrorIn) {
		super.addBox(offX, offY, offZ, (int)width, (int)height, (int)depth, scaleFactor, mirrorIn);
	}

}
