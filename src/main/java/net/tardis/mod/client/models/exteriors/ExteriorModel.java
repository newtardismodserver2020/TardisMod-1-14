package net.tardis.mod.client.models.exteriors;

import net.minecraft.client.renderer.model.Model;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public abstract class ExteriorModel extends Model{
	
	abstract void render(ExteriorTile exterior);

}
