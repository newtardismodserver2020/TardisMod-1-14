package net.tardis.mod.client.renderers.layers;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.player.AbstractClientPlayerEntity;
import net.minecraft.client.renderer.entity.IEntityRenderer;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.client.renderer.entity.model.PlayerModel;
import net.minecraftforge.client.event.RenderWorldLastEvent;

/**
 * Created by Swirtzly
 * on 25/03/2020 @ 12:59
 */
public class SonicLaserRenderLayer extends LayerRenderer<AbstractClientPlayerEntity, PlayerModel<AbstractClientPlayerEntity>> {

    private final IEntityRenderer<AbstractClientPlayerEntity, PlayerModel<AbstractClientPlayerEntity>> renderPlayer;

    public SonicLaserRenderLayer(IEntityRenderer<AbstractClientPlayerEntity, PlayerModel<AbstractClientPlayerEntity>> entityRendererIn) {
        super(entityRendererIn);
        this.renderPlayer = entityRendererIn;
    }


    public static void onRenderWorldLast(RenderWorldLastEvent event) {
        if (Minecraft.getInstance().player == null)
            return;

        /*PlayerEntity player = Minecraft.getInstance().player;
        if (Minecraft.getInstance().gameSettings.thirdPersonView == 0 && player.isHandActive() && PlayerHelper.isInHand(player.getActiveHand(), player, TItems.SONIC)) {
            if (SonicCapability.getForStack(player.getActiveItemStack()).orElse(null).getCharge() > 0) {
                if (SonicItem.getCurrentMode(player.getHeldItem(player.getActiveHand())).getRegistryName() == SonicManager.LASER.getSonicType().getRegistryName()) {
                    double distance = player.getPositionVector().add(0, player.getEyeHeight(), 0).distanceTo(Minecraft.getInstance().objectMouseOver.getHitVec());
                    RenderHelper.setupRenderLightning();
                    GlStateManager.translated(0, -player.getEyeHeight() + 1.5f - (player.isSneaking() ? 0.3 : 0), 0);
                    GlStateManager.rotated(-player.rotationYaw, 0, 1, 0);
                    GlStateManager.rotated(player.rotationPitch, 1, 0, 0);
                    Vec3d start = new Vec3d(-0.1F, 0, 0.3);
                    Vec3d end = start.add(0, 0, distance);
                    RenderHelper.drawGlowingLine(start, end, player.world.rand.nextFloat() * 0.2F, new Vec3d(0.93f, 0.61f, 0), 1);
                    RenderHelper.finishRenderLightning();
                }
            }
        }*/
    }

    @Override
    public boolean shouldCombineTextures() {
        return false;
    }

    @Override
    public void render(AbstractClientPlayerEntity entitylivingbaseIn, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch, float scaleIn) {
        /*double distance = entitylivingbaseIn.getPositionVector().add(0, entitylivingbaseIn.getEyeHeight(), 0).distanceTo(Minecraft.getInstance().objectMouseOver.getHitVec());
        if (entitylivingbaseIn.isHandActive() && PlayerHelper.isInHand(entitylivingbaseIn.getActiveHand(), entitylivingbaseIn, TItems.SONIC)) {
            if (SonicCapability.getForStack(entitylivingbaseIn.getActiveItemStack()).orElse(null).getCharge() > 0) {
                if (SonicItem.getCurrentMode(entitylivingbaseIn.getHeldItem(entitylivingbaseIn.getActiveHand())).getRegistryName() == SonicManager.LASER.getSonicType().getRegistryName()) {
                    RenderHelper.setupRenderLightning();

                    if (entitylivingbaseIn.getPrimaryHand() == HandSide.RIGHT) {
                        renderPlayer.getEntityModel().bipedRightArm.postRender(scaleIn);
                    } else {
                        renderPlayer.getEntityModel().bipedLeftArm.postRender(scaleIn);
                    }
                    GlStateManager.translated(-0.2, 0.5, 0);
                    Vec3d start = new Vec3d(0.1F, scaleIn, 0);
                    Vec3d end = start.add(0, scaleIn, -distance);
                    RenderHelper.drawGlowingLine(start, end, 0.5F, new Vec3d(0.93f, 0.61f, 0), 1);
                    RenderHelper.finishRenderLightning();
                }
            }
        }*/
    }

}
